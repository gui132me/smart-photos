import _thread
import csv
import math
import sqlite3
import unidecode
import reverse_geocoder as rg


database_exists = False
COUNTRY_CC = None

def insert_query(lat, lon):
    return f'INSERT INTO LOCATION (LAT, LON, LOCAL) VALUES ({remove_precision(lat)}, {remove_precision(lon)}, ?);'


def insert_query_with_search(lat, lon):
    return f'INSERT INTO LOCATION (LAT, LON, LOCAL, SEARCH, CITY, STATE, CC) ' \
           f'VALUES ({remove_precision(lat)}, {remove_precision(lon)}, ?, ?, ?, ?, ?);'


def find_by_coord_query(lat, lon):
    return f'SELECT LOCAL FROM LOCATION WHERE LAT=\'{remove_precision(lat)}\' AND LON=\'{remove_precision(lon)}\''


def suggestions_query(text):
    query = f'SELECT LAT, LON, LOCAL FROM LOCATION WHERE SEARCH IS NOT NULL AND instr(SEARCH, \"{text}\")>0' \
           f' ORDER BY instr(SEARCH, \"{text}\") ASC LIMIT 10;'
    return query


def remove_precision(number):
    return truncate(number, 3)


def truncate(number, digits):
    stepper = 10.0 ** digits
    return math.trunc(stepper * number) / stepper


def normalize_text(text):
    return unidecode.unidecode(text).replace(',', '').replace('\'', '').replace('\"', '').lower()


def concat_text(city, region, state, country):
    itens = []
    if city:
        itens.append(city)
    if region and normalize_text(region) != normalize_text(city):
        itens.append(region)
    if state:
        itens.append(state)
    itens.append(country)
    return ', '.join(itens)


def check_database():
    global database_exists
    if database_exists:
        return
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    query = "SELECT name FROM sqlite_master WHERE type='table' AND name='LOCATION';"
    result = cursor.execute(query)
    for _ in result:
        database_exists = True
        return
    conn.commit()
    cursor.close()
    create_database()
    database_exists = True


def create_database():
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS LOCATION
                     ( ID INTEGER PRIMARY KEY NOT NULL,
                     LAT REAL NOT NULL,
                     LON REAL NOT NULL,
                     CITY TEXT,
                     STATE TEXT,
                     CC TEXT,
                     COUNTRY TEXT,
                     LOCAL TEXT NOT NULL,
                     SEARCH TEXT
                     );''')
    with open('./resources/rg_cities1000.csv', 'r') as _filehandler:
        for row in csv.DictReader(_filehandler):
            lat, lon = (float(row["lat"]), float(row["lon"]))
            text = concat_text(row["name"], row['admin2'], row['admin1'], row["cc"])
            search = normalize_text(text)
            query = insert_query_with_search(remove_precision(lat), remove_precision(lon))
            cursor.execute(query, [text, search, row["name"], row['admin1'], row["cc"]])
    with open('./resources/country_codes_csv.csv', 'r') as _filehandler:
        for row in csv.DictReader(_filehandler):
            cc = row["Code"]
            country = row["Name"]
            query = 'UPDATE LOCATION SET COUNTRY=? WHERE CC=?'
            cursor.execute(query, [country, cc])
    conn.commit()
    cursor.close()


def get_suggestions(text):
    check_database()
    text = normalize_text(text)
    query = suggestions_query(text)
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    results = cursor.execute(query)
    return results


def cc_to_country(country):
    global COUNTRY_CC
    if not COUNTRY_CC:
        COUNTRY_CC = {}
        conn = sqlite3.connect('database.sqlite')
        cursor = conn.cursor()
        query = 'SELECT DISTINCT COUNTRY, CC FROM LOCATION'
        results = cursor.execute(query)
        for row in results:
            COUNTRY_CC[row[1]] = row[0]
        conn.commit()
        cursor.close()
    return COUNTRY_CC[country]


def coord_to_text(lat, lon):
    check_database()
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    query = find_by_coord_query(lat, lon)
    database_result = cursor.execute(query)
    for row in database_result:
        text = row[0]
        conn.commit()
        cursor.close()
        return text
    geo_result = rg.search((lat, lon))[0]
    text = concat_text(geo_result["name"], geo_result['admin2'], geo_result['admin1'], geo_result["cc"])
    query = insert_query(lat, lon)
    cursor.execute(query, [text])
    conn.commit()
    cursor.close()
    return text


def start():
    check_database()
    _thread.start_new_thread(lambda: coord_to_text(0, 0), ())