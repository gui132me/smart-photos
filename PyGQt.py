import _thread
import sqlite3
import sys
import time

import unidecode
from PyQt4 import QtCore
from PyQt4.Qt import Qt, QObject, QApplication, QLineEdit, QCompleter
from PyQt4.Qt import QStringListModel, SIGNAL
from PyQt4.QtCore import QThread
from PyQt4.QtGui import QWidget, QVBoxLayout, QLabel
from numpy import unicode

import location_service

TAGS = ['Dayane', 'Guilherme', 'Bauru', 'Paulistânia', 'Celular', 'Lily', 'Fiuk']


class CompleterLineEdit(QLineEdit):
    clicked = QtCore.pyqtSignal()

    def __init__(self, *args):
        QLineEdit.__init__(self, *args)
        QObject.connect(self, SIGNAL('textChanged(QString)'), self.text_changed)

    def mousePressEvent(self, event):
        super(CompleterLineEdit, self).mousePressEvent(event)
        self.clicked.emit()

    def text_changed(self, text):
        all_text = unicode(text)
        text = all_text[:self.cursorPosition()]
        prefix = text.split(',')[-1].strip()

        text_tags = []
        for t in all_text.split(','):
            t1 = unicode(t).strip()
            if t1 != '':
                text_tags.append(t)
        text_tags = list(set(text_tags))
        self.emit(SIGNAL('text_changed(PyQt_PyObject, PyQt_PyObject)'),
                  text_tags, prefix)

    def complete_text(self, text):
        cursor_pos = self.cursorPosition()
        before_text = unicode(self.text())[:cursor_pos]
        after_text = unicode(self.text())[cursor_pos:]
        prefix_len = len(before_text.split(',')[-1].strip())
        self.setText('%s%s, %s' % (before_text[:cursor_pos - prefix_len], text,
                                   after_text))
        self.setCursorPosition(cursor_pos - prefix_len + len(text) + 2)


class TagsCompleter(QCompleter):

    def __init__(self, parent, all_tags):
        QCompleter.__init__(self, all_tags, parent)
        self.all_tags = set(all_tags)

    def update(self, text_tags, completion_prefix):
        tags = list(self.all_tags.difference(text_tags))
        model = QStringListModel(tags, self)
        self.setModel(model)

        self.setCompletionPrefix(completion_prefix)
        if completion_prefix.strip() != '':
            self.complete()


class SearchList(QWidget):
    def __init__(self, list_name, initial_list):
        QWidget.__init__(self)
        layout = QVBoxLayout()
        self.setLayout(layout)
        label = QLabel(list_name)
        layout.addWidget(label)
        self.edit = CompleterLineEdit()
        self.edit.setFixedWidth(200)
        self.edit.setContentsMargins(0, 0, 17, 2)
        layout.addWidget(self.edit)
        self.completer = TagsCompleter(self.edit, initial_list)
        self.edit.clicked.connect(self.completer.complete)
        self.completer.setCaseSensitivity(Qt.CaseInsensitive)
        QObject.connect(self.edit, SIGNAL('text_changed(PyQt_PyObject, PyQt_PyObject)'), self.completer.update)
        QObject.connect(self.completer, SIGNAL('activated(QString)'), self.edit.complete_text)
        self.completer.setWidget(self.edit)
        self.update_trigger = 0
        self.update_query = ''

    def update_list(self, new_list):
        self.completer.deleteLater()
        self.completer = TagsCompleter(self.edit, new_list)
        self.edit.clicked.connect(self.completer.complete)
        self.completer.setCaseSensitivity(Qt.CaseInsensitive)
        QObject.connect(self.edit, SIGNAL('text_changed(PyQt_PyObject, PyQt_PyObject)'), self.completer.update)
        QObject.connect(self.completer, SIGNAL('activated(QString)'), self.edit.complete_text)
        self.completer.setWidget(self.edit)

    def enable_database(self, database_column, table):
        self.edit.textChanged.connect(self.start_updating)
        self.connect(self, SIGNAL('update_completer(QString)'), self.update_completer)
        self.update_query = f'SELECT DISTINCT {database_column} FROM {table} WHERE SEARCH IS NOT NULL AND' \
                            f' instr(LOWER({database_column}), ?)>0 ORDER BY instr({database_column}, ?) ASC LIMIT 10;'

    def start_updating(self, text):
        thr = QThread(self)
        thr.run = lambda: self.wait_update(text)
        thr.start()

    def wait_update(self, text):
        self.update_trigger = self.update_trigger + 1
        time.sleep(0.3)
        self.update_trigger = self.update_trigger - 1
        if self.update_trigger == 0:
            self.emit(SIGNAL('update_completer(QString)'), text)

    def update_completer(self, text):
        print('hahaha')
        if text.find(',') != -1:
            text = text.split(',').pop()[1:]
        text = normalize_text(text)
        conn = sqlite3.connect('database.sqlite')
        cursor = conn.cursor()
        new_list = []
        res = cursor.execute(self.update_query, [text, text])
        for row in res:
            print(row[0])
            new_list.append(row[0])
        self.update_list(new_list)
        self.completer.complete()


def normalize_text(text):
    return unidecode.unidecode(text).replace(',', '').replace('\'', '').replace('\"', '').lower()


def main():
    app = QApplication(sys.argv)

    editor = CompleterLineEdit()

    completer = TagsCompleter(editor, TAGS)
    completer.setCaseSensitivity(Qt.CaseInsensitive)

    QObject.connect(editor, SIGNAL('text_changed(PyQt_PyObject, PyQt_PyObject)'), completer.update)
    QObject.connect(completer, SIGNAL('activated(QString)'), editor.complete_text)

    completer.setWidget(editor)

    editor.show()

    return app.exec_()


if __name__ == '__main__':
    main()
