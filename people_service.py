import os
import pickle
import shutil

from classes.GImage import GImage
import face_rec as fr
import sqlite3

from classes.Person import Person

event_handler = None
faces = None


def create_database():
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS PERSON
                             ( ID INTEGER PRIMARY KEY NOT NULL,
                             SAVED INTEGER,
                             NAME TEXT NOT NULL
                             );''')
    try:
        cursor.execute("INSERT INTO PERSON (ID, SAVED, NAME) VALUES (0, 0, 'Desconhecido')")
    except:
        pass
    cursor.execute('''CREATE TABLE IF NOT EXISTS RECOGNITION
                     ( RECOGNITION_ID INTEGER PRIMARY KEY NOT NULL,
                     IMAGE TEXT NOT NULL,
                     PERSON_ID INTEGER,
                     FOREIGN KEY (PERSON_ID) REFERENCES PERSON(ID)  
                     );''')
    conn.commit()
    cursor.close()


def get_saved_people():
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    query = 'SELECT ID, NAME FROM PERSON WHERE SAVED = 1'
    result = cursor.execute(query)
    people = []
    for row in result:
        people.append(Person(row[0], row[1]))
    conn.commit()
    cursor.close()
    return people


def save():
    f = open('./faces/faces.pckl', 'wb')
    pickle.dump(faces, f)
    f.close()


def check_system():
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    query = 'SELECT IMAGE FROM RECOGNITION'
    result = cursor.execute(query)
    print(result)
    for row in result:
        image = row[0]
        print('checking people', image)
        if not os.path.exists(image):
            delete_cursor = conn.cursor()
            query = 'DELETE FROM RECOGNITION WHERE IMAGE=?'
            delete_cursor.execute(query, [image])
            print(image, 'deleted')
    conn.commit()
    cursor.close()


def start(_event_handler):
    global event_handler
    global faces
    event_handler = _event_handler

    # listener.connect(event_handler, SIGNAL('image_renamed(QStringList)'), rename_file)
    create_database()
    check_system()
    try:
        f = open('./faces/faces.pckl', 'rb')
        faces = pickle.load(f)
        f.close()
    except (OSError, IOError):
        faces = {}
    saved_people = get_saved_people()
    for person in saved_people:
        if person.id not in faces:
            faces[person.id] = fr.get_first_encoding(f'./faces/{person.id}.jpg')
    save()


def reset_rec():
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    query = 'DELETE FROM RECOGNITION WHERE IMAGE IN (SELECT DISTINCT IMAGE FROM RECOGNITION WHERE PERSON_ID = 0)'
    cursor.execute(query)
    conn.commit()
    cursor.close()


def add(image_path, name):
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    query = 'INSERT INTO PERSON (NAME, SAVED) VALUES (?, NULL)'
    cursor.execute(query, [name])
    query = 'SELECT ID, NAME FROM PERSON WHERE SAVED IS NULL'
    res = cursor.execute(query)
    new_person = None
    for row in res:
        new_person = Person(row[0], row[1])
    query = 'UPDATE PERSON SET SAVED=1 WHERE ID=?'
    cursor.execute(query, [new_person.id])
    conn.commit()
    cursor.close()
    faces[new_person.id] = fr.get_first_encoding(image_path)
    print(f'{new_person.id}.jpg')
    shutil.copy(image_path, f'faces/{new_person.id}.jpg')
    reset_rec()
    save()
    return new_person


def rename(person, new_name):
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    query = f'INSERT INTO PERSON (NAME,SAVED) VALUES(?,0)'
    cursor.execute(query, [person.name])
    query = f'UPDATE PERSON SET NAME=? WHERE ID={person.id}'
    cursor.execute(query, [new_name])
    conn.commit()
    cursor.close()


def get_occurrences(person_id):
    query = f'SELECT IMAGE FROM RECOGNITION WHERE PERSON_ID = {person_id}'
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    images = []
    results = cursor.execute(query)
    for row in results:
        images.append(row[0])
    conn.commit()
    cursor.close()
    return images


def change_face(image_path, person):
    faces[person.id] = fr.get_first_encoding(image_path)
    reset_rec()
    save()


def change_photo(person_id, image_path):
    faces[person_id] = fr.get_first_encoding(image_path)
    os.remove(f'faces/{person_id}.jpg')
    shutil.copy(image_path, f'faces/{person_id}.jpg')


def rename_file(paths):
    print('rename people', paths)
    old_file_path = paths[0]
    new_file_path = paths[1]
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    query = 'UPDATE RECOGNITION SET IMAGE = ? WHERE IMAGE = ?'
    cursor.execute(query, [new_file_path, old_file_path])
    conn.commit()
    cursor.close()


def find(image_path):
    query = 'SELECT NAME FROM RECOGNITION, PERSON WHERE IMAGE = ? AND (PERSON_ID = ID OR PERSON_ID IS NULL)'
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    people = []
    results = cursor.execute(query, [image_path])
    for row in results:
        if row[0] is None:
            return people, False
        people.append(row[0])
    conn.commit()
    cursor.close()
    if len(people):
        return people, False

    people = []
    saved_people_dict = {}
    saved_people = get_saved_people()
    for people_obj in saved_people:
        saved_people_dict[people_obj.id] = people_obj
    people_ids = fr.detect(image_path, faces)
    for person_id in people_ids:
        if person_id == 0:
            people.append(Person(0, 'Desconhecido'))
        else:
            people.append(saved_people_dict[person_id])
    insert_people(image_path, people)
    people_names = [p.name for p in people]
    return people_names, True


def insert_people(image_path, people):
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    if len(people):
        query = 'INSERT INTO RECOGNITION (IMAGE, PERSON_ID) VALUES '
        first_tuple = True
        tuples_args = []
        for person in people:
            if first_tuple:
                first_tuple = False
                query = query + '(?, ?)'
            else:
                query = query + ', (?, ?)'
            tuples_args.append(image_path)
            tuples_args.append(person.id)
        query = query + ';'
        cursor.execute(query, tuples_args)
    else:
        query = 'INSERT INTO RECOGNITION (IMAGE, PERSON_ID) VALUES (?, NULL)'
        cursor.execute(query, [image_path])
    conn.commit()
    cursor.close()


def add_person(name, image=None):
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    if not image:
        query = 'INSERT INTO PERSON (NAME, SAVED) VALUES (?, 0)'
        cursor.execute(query, [name])
    else:
        query = 'INSERT INTO PERSON (NAME, SAVED) VALUES (?, 1)'
        cursor.execute(query, [name])
    conn.commit()
    cursor.close()


def get_people():
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    query = 'SELECT DISTINCT NAME FROM PERSON'
    result = cursor.execute(query)
    people = []
    for row in result:
        people.append(row[0])
    conn.commit()
    cursor.close()
    return people


# def all_people():

    # return fr.get_names() + get_people()


# return the usage of a person
def usage(person):
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    query = 'SELECT IMAGE FROM RECOGNITION WHERE PERSON_ID = ?'
    result = cursor.execute(query, [person.id])
    images = []
    for row in result:
        images.append(row[0])
    conn.commit()
    cursor.close()
    return images


def delete(person, image_path=None):
    if image_path:
        image = GImage(image_path)
        image.delete_keyword(person.name)
        image.add_keyword('Desconhecido')
        image.deleteLater()

        conn = sqlite3.connect('database.sqlite')
        cursor = conn.cursor()
        query = 'UPDATE RECOGNITION SET PERSON_ID = 0 WHERE PERSON_ID = ? AND IMAGE = ?'
        cursor.execute(query, (person.id, image_path))
        conn.commit()
        cursor.close()
    else:
        conn = sqlite3.connect('database.sqlite')
        cursor = conn.cursor()
        query = 'UPDATE RECOGNITION SET PERSON_ID = 0 WHERE PERSON_ID = ?'
        cursor.execute(query, [person.id])
        query = 'UPDATE PERSON SET SAVED = 0 WHERE ID = ?'
        cursor.execute(query, [person.id])
        conn.commit()
        cursor.close()
        os.remove(f'faces/{person.id}.jpg')
        del faces[person.id]
        save()


# def add(name, image_path):
#     image = GImage(image_path)
#     image.delete_keyword(name)
#     image.deleteLater()
#
#     conn = sqlite3.connect('database.sqlite')
#     cursor = conn.cursor()
#     query = 'INSET INTO RECOGNITION () VALUES NAME = ? AND IMAGE = ?'
#     cursor.execute(query, (name, image_path))
#     conn.commit()
#     cursor.close()
