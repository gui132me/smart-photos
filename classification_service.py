import sqlite3

import keras
from keras.layers import Dense
from keras.models import Model
from keras.models import load_model
import pickle
import os
import glob
import numpy as np
import random

from keras.optimizers import Adam
from keras.preprocessing.image import ImageDataGenerator, load_img
import shutil
from shutil import copyfile
import glob

TRAIN = 0.85


def create_database():
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS CLASSIFICATION
                     ( IMAGE TEXT PRIMARY KEY NOT NULL,
                     CLASS_NAME TEXT NOT NULL
                     );''')
    conn.commit()
    cursor.close()


def check_images():
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    query = 'SELECT IMAGE FROM CLASSIFICATION'
    result = cursor.execute(query)
    for row in result:
        image = row[0]
        if not os.path.exists(image):
            delete_cursor = conn.cursor()
            query = 'DELETE FROM CLASSIFICATION WHERE IMAGE=?'
            delete_cursor.execute(query, [image])
    conn.commit()
    cursor.close()


def reset_database():
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    query = 'DELETE FROM CLASSIFICATION'
    cursor.execute(query)
    conn.commit()
    cursor.close()


def start():
    create_database()
    check_images()
    global model
    model = load_model('classification/model.h5')
    global index_class_dict
    index_class_dict = {}
    classes_file = open('resources/classes.pckl', 'rb')
    classes = pickle.load(classes_file)
    classes_file.close()
    idx = 0
    for (model_name, class_folder_path) in classes:
        index_class_dict[idx] = model_name
        idx = idx + 1
    return model


def train(classes):
    shutil.rmtree('classification/train', ignore_errors=True)
    shutil.rmtree('classification/validation', ignore_errors=True)
    os.mkdir('classification/train')
    n_train = 0
    n_valid = 0
    os.mkdir('classification/validation')
    for (model_name, class_folder_path) in classes:
        os.mkdir(f'classification/train/{model_name}')
        os.mkdir(f'classification/validation/{model_name}')
        images = glob.glob1(class_folder_path, "*.jpg") + glob.glob1(class_folder_path, "*.png") + \
                 glob.glob1(class_folder_path, "*.jpeg")
        total = len(images)
        train_files = int(total * TRAIN)
        for image in images:
            if train_files > 0:
                n_train = n_train + 1
                copyfile(f'{class_folder_path}/{image}', f'classification/train/{model_name}/{image}')
            else:
                n_valid = n_valid + 1
                copyfile(f'{class_folder_path}/{image}', f'classification/validation/{model_name}/{image}')
            train_files = train_files - 1
        print(model_name)

    train_batches = ImageDataGenerator(preprocessing_function=keras.applications.mobilenet.preprocess_input) \
        .flow_from_directory('classification/train', target_size=(224, 224), batch_size=10)
    valid_batches = ImageDataGenerator(preprocessing_function=keras.applications.mobilenet.preprocess_input) \
        .flow_from_directory('classification/validation', target_size=(224, 224), batch_size=10)

    mobile = keras.applications.mobilenet.MobileNet()

    x = mobile.layers[-6].output
    predictions = Dense(len(classes), activation='softmax')(x)
    global model
    model = Model(inputs=mobile.input, outputs=predictions)

    for layer in model.layers[:-23]:
        layer.trainable = False

    model.compile(Adam(lr=.0001), loss='categorical_crossentropy', metrics=['accuracy'])

    model.fit_generator(train_batches, steps_per_epoch=int(n_train / 10),
                        validation_data=valid_batches, validation_steps=int(n_valid / 10),
                        epochs=60, verbose=2)

    model.save('classification/model.h5')

    new_classes = [None]*len(classes)

    class_index_dict = train_batches.class_indices

    for (model_name, class_folder_path) in classes:
        new_classes[class_index_dict[model_name]] = (model_name, class_folder_path)

    classes_file = open('resources/classes.pckl', 'wb')
    pickle.dump(classes, classes_file)
    classes_file.close()

    reset_database()

    global index_class_dict
    idx = 0
    for (model_name, class_folder_path) in classes:
        index_class_dict[idx] = model_name
        idx = idx + 1

    return model


def save_classification(image_path, class_name):
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    query = 'INSERT INTO CLASSIFICATION (IMAGE, CLASS_NAME) VALUES (?, ?)'
    cursor.execute(query, [image_path, class_name])
    conn.commit()
    cursor.close()


def classify(image_path):
    query = 'SELECT CLASS_NAME FROM CLASSIFICATION WHERE IMAGE = ?'
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    results = cursor.execute(query, [image_path])
    for row in results:
        return row[0], False
    conn.commit()
    cursor.close()

    print(index_class_dict)
    img = load_img(image_path, target_size=(224, 224))
    img = np.expand_dims(img, axis=0)
    preprocessed_image = keras.applications.mobilenet.preprocess_input(img)
    prediction = model.predict(preprocessed_image)[0].tolist()
    print(prediction)
    max_val = max(prediction)
    idx = prediction.index(max_val)
    save_classification(image_path, index_class_dict[idx])
    return index_class_dict[idx], True


def usage(class_name):
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    query = 'SELECT IMAGE FROM CLASSIFICATION WHERE CLASS_NAME = ?'
    result = cursor.execute(query, [class_name])
    images = []
    for row in result:
        images.append(row[0])
    conn.commit()
    cursor.close()
    return images


def test():
    test_batches = ImageDataGenerator(preprocessing_function=keras.applications.mobilenet.preprocess_input) \
        .flow_from_directory('classification/test', target_size=(224, 224), batch_size=10, shuffle=False)

    predictions = model.predict_generator(test_batches, steps=1, verbose=1)