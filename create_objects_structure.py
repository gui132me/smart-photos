import json


def create():
    f = open('resources/object_detection/yolov3_open_images.txt', 'r', encoding='utf-8')
    en_names = f.read().splitlines()
    f = open('resources/object_detection/yolov3_open_images_ptBR.txt', 'r', encoding='utf-8')
    pt_names = f.read().splitlines()
    f_missing = open('resources/object_detection/missing_objects.txt', 'w', encoding='utf-8')
    errors = 0
    f = open('resources/object_detection/missing_objects_pt.txt', 'r', encoding='utf-8')
    pt_missing_names = f.read().splitlines()
    with open('image-labels-transformed.json', 'r') as f:
        itens_dict = json.load(f)
    matchs = 0
    for item_object in itens_dict:
        if item_object['name'] == 'Object':
            item_object['name'] = 'Todos'
        else:
            en_name = item_object['name']
            pt_name = en_name
            unmatch = True
            for i in range(len(en_names)):
                if en_names[i].lower() == en_name.lower():
                    matchs = matchs + 1
                    pt_name = pt_names[i]
                    unmatch = False
                    item_object['class'] = True
                    item_object['selection_id'] = i
            if unmatch:
                item_object['class'] = False
                item_object['selection_id'] = -1
                pt_name = pt_missing_names[errors]
                errors = errors + 1
                print('ocorreu um erro em', en_name, 'mudaremos para', pt_name)
                f_missing.write(en_name + '\n')
            item_object['name'] = pt_name

    print(f'houve {matchs} matchs e {errors} erros (temos {len(pt_missing_names)} palavras para traduzir)')
    f_missing.close()
    # itens_dict.sort(key=lambda x: x['name'])

    with open('image-labels-structure-pt.json', 'w') as f:
        json.dump(itens_dict, f)

    f.close()
