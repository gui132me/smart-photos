from PyQt4.QtCore import QSize, Qt, SIGNAL
from PyQt4.QtGui import QWidget, QLineEdit, QHBoxLayout, QIcon, QPushButton, QCursor, QCompleter, QStringListModel
import location_service


class GLocalChanger(QWidget):
    def __init__(self):
        super(GLocalChanger, self).__init__()

        # Layout
        layout = QHBoxLayout()
        layout.setMargin(0)
        self.setLayout(layout)

        self.edit = QLineEdit(self)
        self.edit.setFixedWidth(335)
        self.edit.setReadOnly(True)
        self.edit.setStyleSheet("QLineEdit {font: 15pt Comic Sans MS; border: none;}")
        self.edit.setCursor(QCursor(Qt.ArrowCursor))
        self.edit.setMouseTracking(True)
        self.edit.cursorPositionChanged.connect(lambda event: self.edit.setCursor(QCursor(Qt.ArrowCursor)))
        layout.addWidget(self.edit)

        icon = QIcon()
        icon.addFile('resources/icons/edit.png', QSize(20, 20))
        self.button = QPushButton()
        self.button.setIcon(icon)
        self.button.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.button)

        icon = QIcon()
        icon.addFile('resources/icons/cancel.png', QSize(20, 20))
        self.cancel_button = QPushButton()
        self.cancel_button.setIcon(icon)
        self.cancel_button.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.cancel_button)
        self.cancel_button.hide()

        layout.addStretch()

        self.completer = QCompleter()
        self.edit.setCompleter(self.completer)
        self.completer_model = QStringListModel()
        self.completer.setModel(self.completer_model)
        self.completer.setCaseSensitivity(Qt.CaseInsensitive)

        # Functionality
        self.is_editing = False
        self.saved_text = ''
        self.button.clicked.connect(lambda event: self.toggle_editing())
        self.cancel_button.clicked.connect(lambda event: self.cancel_editing())
        self.edit.textChanged.connect(self.changing_text)
        self.edit.setFixedWidth(335)

        self.valid_places = []
        self.valid_places_coord = []
        self.completer_model.setStringList(self.valid_places)
        self.trigger_query = 0

    def reset(self):
        self.is_editing = False
        self.saved_text = ''
        self.trigger_query = 0

    def changing_text(self, text):
        if text != self.saved_text and text not in self.valid_places:
            print('textin', text)
            self.button.setEnabled(False)
        else:
            self.button.setEnabled(True)
        if self.is_editing:
            # _thread.start_new_thread(lambda: self.update_cities(text), ())
            self.update_cities(text)

    def update_cities(self, text):
        self.trigger_query = self.trigger_query + 1
        # time.sleep(0.1)
        if text not in self.valid_places:
            self.trigger_query = self.trigger_query - 1
        if self.trigger_query is 0:
            self.valid_places = []
            self.valid_places_coord = []
            print('asyncronouslly updating', text)
            res = location_service.get_suggestions(text)
            for row in res:
                text = row[2]
                self.valid_places.append(text)
                self.valid_places_coord.append((row[0], row[1]))

            self.completer_model.setStringList(self.valid_places)
        else:
            self.trigger_query = self.trigger_query - 1

    def setText(self, text):
        self.saved_text = text
        self.edit.setText(text)

    def setLocal(self, text):
        self.setText(text)
        self.edit.setFixedWidth(302)
        icon = QIcon()
        icon.addFile('resources/icons/garbage.png', QSize(20, 20))
        self.cancel_button.setIcon(icon)
        self.cancel_button.show()
        icon = QIcon()
        icon.addFile('resources/icons/edit.png', QSize(20, 20))
        self.button.setIcon(icon)

    def removeLocal(self):
        icon = QIcon()
        icon.addFile('resources/icons/edit.png', QSize(20, 20))
        self.button.setIcon(icon)
        self.cancel_button.hide()
        self.setText('Não Definido')
        self.edit.setStyleSheet("QLineEdit {font: 15pt Comic Sans MS; border: none;}")
        self.edit.setReadOnly(True)
        self.edit.setFixedWidth(335)

    def toggle_editing(self):
        if self.is_editing:
            # self.save_local
            print('image_renaming')
            if self.saved_text != self.edit.text():
                self.saved_text = self.edit.text()
                print('image_renamed(...)', self.saved_text,
                      self.valid_places_coord[self.valid_places.index(self.saved_text)])
                a, b = self.valid_places_coord[self.valid_places.index(self.saved_text)]
                self.emit(SIGNAL('coord_changed(QString, PyQt_PyObject, PyQt_PyObject)'), self.saved_text, a, b)

            icon = QIcon()
            icon.addFile('resources/icons/garbage.png', QSize(20, 20))
            self.cancel_button.setIcon(icon)
            self.stop_editing()
            self.cancel_button.show()
            if self.saved_text is 'Não Definido':
                self.cancel_button.hide()
                icon = QIcon()
                icon.addFile('resources/icons/garbage.png', QSize(20, 20))
                self.cancel_button.setIcon(icon)
                self.edit.setFixedWidth(335)
        else:
            self.edit.setReadOnly(False)
            self.edit.setStyleSheet("QLineEdit {font: 15pt Comic Sans MS; border: 1px solid black;}")
            self.edit.setFixedWidth(302)
            self.edit.setFocus(True)
            self.cancel_button.show()
            self.is_editing = True
            icon = QIcon()
            icon.addFile('resources/icons/tick.png', QSize(20, 20))
            self.button.setIcon(icon)
            icon = QIcon()
            icon.addFile('resources/icons/cancel.png', QSize(20, 20))
            self.cancel_button.setIcon(icon)

    def stop_editing(self):
        self.edit.setReadOnly(True)
        self.edit.setStyleSheet("QLineEdit {font: 15pt Comic Sans MS; border: 0;}")
        self.cancel_button.hide()
        self.is_editing = False
        icon = QIcon()
        icon.addFile('resources/icons/edit.png', QSize(20, 20))
        self.button.setIcon(icon)

    def cancel_editing(self):
        if self.is_editing:
            self.setText(self.saved_text)
            self.edit.setReadOnly(True)
            self.edit.setStyleSheet("QLineEdit {font: 15pt Comic Sans MS; border: 0;}")
            self.is_editing = False
            if self.saved_text == 'Não Definido':
                self.cancel_button.hide()
                self.edit.setFixedWidth(335)
            else:
                icon = QIcon()
                icon.addFile('resources/icons/garbage.png', QSize(20, 20))
                self.cancel_button.setIcon(icon)
                self.edit.setFixedWidth(302)
            icon = QIcon()
            icon.addFile('resources/icons/edit.png', QSize(20, 20))
            self.button.setIcon(icon)
        else:
            print('delete the locl')
            self.removeLocal()
            self.emit(SIGNAL('coord_changed(QString, PyQt_PyObject, PyQt_PyObject)'), '', 0, 0)
