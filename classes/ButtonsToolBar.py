from PyQt4.QtCore import Qt, QSize, SIGNAL
from PyQt4.QtGui import QIcon, QToolBar, QAction, QPushButton


class Button:
    def __init__(self, image, text, tool_name):
        self.image = image
        self.text = text
        self.event_id = tool_name


def toggle_menu():
    icon = QIcon()
    icon.addFile('resources/icons/menu.png', QSize(50, 50))
    action = QAction(None)
    action.setIcon(icon)
    button = QPushButton()
    button.addAction(action)
    button.show()


class ButtonsToolBar(QToolBar):
    def __init__(self, search_bar):
        super(ButtonsToolBar, self).__init__()
        self.search_bar = search_bar
        self.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.setStyleSheet("QToolButton { margin: 10px };")
        self.setStyleSheet("QToolBar {background-color: white;} QToolButton { margin: 10px; }")
        self.setMovable(False)
        self.addButtons()

    def addButtons(self):
        size = QSize(50, 50)
        buttons = [Button('search.png', 'Pesquisa', 'search'),
                   Button('folder.png', 'Pastas', 'folder'),
                   Button('people.png', 'Pessoas', 'people'),
                   Button('cat.png', 'Objetos', 'objects'),
                   Button('learn.png', 'Ensinar', 'learn'),]
                   # Button('export.png', 'Mover', None)]
        for button in buttons:
            icon = QIcon()
            icon.addFile(f'resources/icons/{button.image}', size)
            action = QAction(icon, button.text, self)
            if button.event_id is None:
                action.setEnabled(False)
            self.connect_action(action, button.event_id)
            self.addAction(action)

    def connect_action(self, action, tool_name):
        if tool_name is 'menu':
            action.triggered.connect(lambda event: toggle_menu())
        elif tool_name is 'search':
            action.triggered.connect(lambda event: self.toggle_search())
        else:
            action.triggered.connect(lambda event: self.change_tool(tool_name))

    def change_tool(self, tool_name):
        self.emit(SIGNAL("folder_selected(QString)"), tool_name)

    def toggle_search(self):
        if self.search_bar.isHidden():
            self.search_bar.show()
        else:
            self.search_bar.hide()