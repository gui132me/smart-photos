import math
import os
from PyQt4.QtCore import Qt, QObject, SIGNAL, QDate
from PyQt4.QtGui import QPixmap, QLabel, QCursor
from iptcinfo3 import IPTCInfo
import re
import piexif
import reverse_geocoder as rg
import _thread

import location_service
import people_service
from location_service import cc_to_country


class GImage(QObject):
    MONTHS = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',
              'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novenbro', 'Dezembro']

    def __init__(self, path, event_handler=None):
        super(GImage, self).__init__()
        self.event_handler = event_handler

        self._path = path

        self._pixmap = None
        self._iptc = IPTCInfo(self._path, force=True)
        try:
            self._keywords = [keyword.decode('utf-8') for keyword in self._iptc['keywords']]
        except:
            self._keywords = []

        # _file = open(path, 'rb')
        # self._exif_tags = exifread.process_file(_file)
        # _file.close()

        print(self.path(),'oppedned',self._keywords)

        self._local = None
        self._exif_dict = None

        if self.extension().lower() == 'jpg' or self.extension().lower() == 'jpeg':
            try:
                self._exif_dict = piexif.load(path)
                # for ifd in ("0th", "Exif", "GPS", "1st"):
                #     for tag in self._exif_dict[ifd]:
                #         print(ifd,piexif.TAGS[ifd][tag]["name"], self._exif_dict[ifd][tag])
                if self.has_date():
                    print('date exif tag', self._exif_dict['Exif'][piexif.ExifIFD.DateTimeOriginal])
                    self._date = self.decode_exif_date()
                else:
                    self._date = None
                gps_keys = [piexif.GPSIFD.GPSLatitudeRef, piexif.GPSIFD.GPSLatitude,
                            piexif.GPSIFD.GPSLongitudeRef, piexif.GPSIFD.GPSLongitude]

                if all(elem in self._exif_dict['GPS'].keys() for elem in gps_keys):
                    latitude = self._exif_dict['GPS'].get(piexif.GPSIFD.GPSLatitude)
                    latitude_ref = self._exif_dict['GPS'].get(piexif.GPSIFD.GPSLatitudeRef).decode('utf-8')
                    longitude = self._exif_dict['GPS'].get(piexif.GPSIFD.GPSLongitude)
                    longitude_ref = self._exif_dict['GPS'].get(piexif.GPSIFD.GPSLongitudeRef).decode('utf-8')
                    lat_value = _convert_to_degress(latitude)
                    if latitude_ref != 'N':
                        lat_value = -lat_value
                    lon_value = _convert_to_degress(longitude)
                    if longitude_ref != 'E':
                        lon_value = -lon_value
                    self._coord = (lat_value, lon_value)
                    print('setting local', self._path)
                    # self._local = None
                    # _thread.start_new_thread(lambda: self.find_local(), ())
                    # self._local = coord_to_text(self._coord)
                    self.find_local()
                    # print(self._local)
                    print('finished')
            except:
                print('exif error on image', self.path())

        self._frame = None
        self._f = None

    def change_gps(self, txt, lat, lon):
        if txt == '':
            self.remove_local()
        else:
            self._local = txt
            if lat < 0:
                self._exif_dict["GPS"][piexif.GPSIFD.GPSLatitudeRef] = 'S'.encode('utf-8')
            else:
                self._exif_dict["GPS"][piexif.GPSIFD.GPSLatitudeRef] = 'N'.encode('utf-8')
            self._exif_dict["GPS"][piexif.GPSIFD.GPSLatitude] = degToDmsRational(lat)
            if lon < 0:
                self._exif_dict["GPS"][piexif.GPSIFD.GPSLongitudeRef] = 'W'.encode('utf-8')
            else:
                self._exif_dict["GPS"][piexif.GPSIFD.GPSLongitudeRef] = 'E'.encode('utf-8')
            self._exif_dict["GPS"][piexif.GPSIFD.GPSLongitude] = degToDmsRational(lon)
            exif_bytes = piexif.dump(self._exif_dict)
            piexif.insert(exif_bytes, self.path())
            self.event_handler.emit(SIGNAL('local_changed(QString, QString)'), self.path(), self._local)
            print('ready to save', txt, lat, lon)

    def find_local(self):
        self._local = location_service.coord_to_text(self._coord[0], self._coord[1])

    def set_local(self, local):
        self._local = local

    def remove_local(self):
        self._local = None
        del self._exif_dict['GPS'][piexif.GPSIFD.GPSLatitude]
        del self._exif_dict['GPS'][piexif.GPSIFD.GPSLatitudeRef]
        del self._exif_dict['GPS'][piexif.GPSIFD.GPSLongitude]
        del self._exif_dict['GPS'][piexif.GPSIFD.GPSLongitudeRef]
        exif_bytes = piexif.dump(self._exif_dict)
        piexif.insert(exif_bytes, self.path())
        self.event_handler.emit(SIGNAL('local_changed(QString, QString)'), self.path(), '')
        # 'image_renamed(QString, PyQt_PyObject, PyQt_PyObject)'

    def has_local(self):
        return self._local

    def local(self):
        # text = self._local["name"]
        # if self._local["admin1"]:
        #     text = f'{text}, {self._local["admin1"]}'
        # if self._local["admin2"] and self._local["admin2"] != self._local["name"]:
        #     text = f'{text}, {self._local["admin2"]}'
        # text = f'{text}, {self._local["cc"]}'

        # text = coord_to_text(self._local)

        return self._local

    def exif_tags(self):
        return self._exif_tags

    def has_date(self):
        return self._exif_dict and piexif.ExifIFD.DateTimeOriginal in self._exif_dict['Exif']

    def encode_exif_date(self):
        date = self.date()
        formatted_date = f'{date.year()}:{date.month()}:{date.day()} 00:00:00'
        self._exif_dict['Exif'][piexif.ExifIFD.DateTimeOriginal] = formatted_date.encode('utf-8')
        exif_bytes = piexif.dump(self._exif_dict)
        piexif.insert(exif_bytes, self.path())

    def decode_exif_date(self):
        data = self._exif_dict['Exif'][piexif.ExifIFD.DateTimeOriginal].decode('utf-8').split(' ')[0].split(':')
        try:
            ano = data[0]
            mes = data[1]
            dia = data[2]
        except:
            data = self._exif_dict['Exif'][piexif.ExifIFD.DateTimeDigitized]
            ano = data[0]
            mes = data[1]
            dia = data[2]

        return QDate(int(ano), int(mes), int(dia))

    def date(self):
        return self._date

    def path(self):
        return self._path

    def folder(self):
        return os.path.dirname(self.path())

    def extension(self):
        return self.path().split('.').pop()

    def logical_rename(self, name):
        self._path = name

    def rename(self, new_name):
        old_file_path = self.path()
        new_file_path = f'{self.folder()}/{new_name}.{self.extension()}'
        os.rename(old_file_path, new_file_path)
        self.logical_rename(new_file_path)
        self.event_handler.emit_signal('image_renamed(QStringList)', [old_file_path, new_file_path])

    def change_date(self, date):
        self.event_handler.emit(SIGNAL('date_changed(QString, QDate)'), self.path(), date)
        self.setDate(date)
        if date.isNull():
            print('removeu')
            del self._exif_dict['Exif'][piexif.ExifIFD.DateTimeOriginal]
            exif_bytes = piexif.dump(self._exif_dict)
            piexif.insert(exif_bytes, self.path())
        else:
            # self.event_handler.emit_signal('image_renamed(QStringList)', [old_file_path, new_file_path])
            self.encode_exif_date()

    def setDate(self, date):
        self._date = date

    def pixmap(self):
        if not self._pixmap:
            self._pixmap = QPixmap(self._path)
        return self._pixmap

    def frame(self):
        if self._frame:
            return self._frame
        self._frame = QLabel()
        self._frame.setCursor(QCursor(Qt.PointingHandCursor))
        pixels = self.pixmap().scaledToHeight(200, Qt.SmoothTransformation)
        self._frame.setPixmap(pixels)
        self.remove_highlight()
        return self._frame

    def remove_highlight(self):
        self._frame.setStyleSheet("QLabel{margin: 3px; border: 3px solid transparent;}")

    def highlight(self, new_detection):
        if new_detection:
            self._frame.setStyleSheet("QLabel{margin: 3px; border: 3px solid blue;}")
        else:
            self._frame.setStyleSheet("QLabel{margin: 3px; border: 3px solid gray;}")

    def destroy(self):
        self._f.close()

    def save_iptc(self):
        try:
            self._iptc.save()
            if os.path.exists(f'{self._path}~'):
                # print('temporary file cleared')
                # os.remove(f'{self._path}~')
                pass
        except Exception as e:
            print('IPTC error saving image', self.path())

    def filter(self, search_texts):
        match = False
        keyword_search = search_texts[0] + search_texts[1]
        keyword_search = [w.lower() for w in keyword_search]
        image_keywords = [ word.lower() for word in self._keywords]
        if not keyword_search or any(elem in image_keywords for elem in keyword_search):
            image_date = self.date()
            days = list(map(int, search_texts[5]))
            if not days or image_date and image_date.day() in days:
                mons = search_texts[6]
                if not mons or image_date and GImage.MONTHS[image_date.month()-1] in mons:
                    yrs = list(map(int, search_texts[7]))
                    if not yrs or image_date and image_date.year() in yrs:
                        cities = search_texts[2]
                        image_local = self.local()
                        if not cities or image_local and image_local.split(', ')[0] in cities:
                            countries = search_texts[4]
                            if not countries or image_local and cc_to_country(image_local.split(', ').pop()) in countries:
                                states = search_texts[3]
                                if not states or image_local and image_local.split(', ')[1] in states:
                                    match = True
        self._frame.setShown(match)

    def add_keywords(self, word_list):
        for word in word_list:
            if word not in self._keywords:
                self._keywords.append(word)
        self.save_keywords()

    def add_keyword(self, word):
        if word not in self._keywords:
            self._keywords.append(word)
            self.save_keywords()

    def delete_keyword(self, word):
        old_keywords = self._keywords
        new_keywords = []
        for w in old_keywords:
            if w != word:
                new_keywords.append(w)
        self._keywords = new_keywords
        self.save_keywords()

    def save_keywords(self):
        self._iptc['keywords'] = [keyword.encode('utf-8') for keyword in self._keywords]
        self.save_iptc()

    def people(self):
        return [person for person in people_service.get_people() if person in self._keywords]

    def objects(self):
        return [obj for obj in self._keywords if obj not in people_service.get_people()]


def _convert_to_degress(value):
    # fração
    d, m, s = value
    # decimal
    d = float(d[0]) / float(d[1])
    m = float(m[0]) / float(m[1])
    s = float(s[0]) / float(s[1])
    return d + (m / 60.0) + (s / 3600.0)


def degToDmsRational(deg_float):
    deg_float = abs(deg_float)
    minFloat = deg_float % 1 * 60
    secFloat = minFloat % 1 * 60
    deg = math.floor(deg_float)
    minu = math.floor(minFloat)
    sec = round(secFloat * 100)
    return ((deg, 1), (minu, 1), (sec, 100))

