import re
import time

from PyQt4.QtCore import Qt, QObject, SIGNAL, QThread, QSize
from PyQt4.QtGui import QToolBar, QVBoxLayout, QWidget, QLabel, QHBoxLayout, QGroupBox, QScrollArea, QPixmap, QIcon, \
    QPushButton, QCheckBox, QSizePolicy

import people_service
from PyGQt import CompleterLineEdit, TagsCompleter, SearchList

event_handler = None

MONTHS = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',
          'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novenbro', 'Dezembro']


class SearchBar(QToolBar):
    def __init__(self, main_window_event_handler, selected_objects):
        super(SearchBar, self).__init__()
        global event_handler
        event_handler = main_window_event_handler
        self.hide()
        self.objects = []
        self.people = []

        self.update_trigger = 0
        self.search_texts = [[]]*8

        self.selected_objects = selected_objects

        self.setMovable(False)
        layout = QHBoxLayout()
        widget = QWidget()
        widget.setLayout(layout)
        # widget.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.horizontalStretch())

        layout_boxes = QVBoxLayout()
        boxes_widget = QWidget()
        boxes_widget.setLayout(layout_boxes)
        layout.addWidget(boxes_widget)

        cb1 = QCheckBox('Palavras-chave')
        cb1.setToolTip('Pesquisa por palavras chave')
        cb1.setChecked(True)
        layout_boxes.addWidget(cb1)

        cb2 = QCheckBox('Data')
        cb2.setToolTip('Pesquisa por dias, meses e anos')
        layout_boxes.addWidget(cb2)

        cb3 = QCheckBox('Localização')
        cb3.setToolTip('Pesquisa por cidades, estados e países')
        layout_boxes.addWidget(cb3)

        keywords_group = QGroupBox('Palavras-chave')
        self.setStyleSheet('QToolBar{border-bottom: 1px solid silver; } QWidget{background-color: white;} QGroupBox{ font: bold; margin-top: 6px; border: 1px solid silver; border-radius: 6px; } QGroupBox::title { subcontrol-origin: margin; left: 7px; padding: 0px 5px 0px 5px; }')
        layout.addWidget(keywords_group)
        layout_local = QHBoxLayout()
        keywords_group.setLayout(layout_local)

        person_search = SearchList('Pessoas:', people_service.get_people())
        layout_local.addWidget(person_search)
        person_search.edit.textChanged.connect(lambda text: self.trigger_change(0, text))

        object_search = SearchList('Objetos:', selected_objects)
        layout_local.addWidget(object_search)
        QObject.connect(event_handler, SIGNAL('objects_changed(QStringList)'),
                        object_search.update_list)
        object_search.edit.textChanged.connect(lambda text: self.trigger_change(1, text))

        local_group = QGroupBox('Local')
        layout_local = QHBoxLayout()
        local_group.setLayout(layout_local)

        local_search = SearchList('Cidades:', ['Bauru', 'Duartina', 'Curitiba'])
        local_search.enable_database('CITY', 'LOCATION')
        layout_local.addWidget(local_search)
        local_search.edit.textChanged.connect(lambda text: self.trigger_change(2, text))

        local_search2 = SearchList('Estados:', ['Sao Paulo', 'Parana', 'Rio de Janeiro'])
        local_search2.enable_database('STATE', 'LOCATION')
        layout_local.addWidget(local_search2)
        local_search2.edit.textChanged.connect(lambda text: self.trigger_change(3, text))

        local_search3 = SearchList('Países:', ['Brazil', 'United States', 'Spain'])
        local_search3.enable_database('COUNTRY', 'LOCATION')
        layout_local.addWidget(local_search3)
        local_search3.edit.textChanged.connect(lambda text: self.trigger_change(4, text))

        date2_group = QGroupBox('Entre Datas')
        layout_date2 = QHBoxLayout()
        date2_group.setLayout(layout_date2)

        icon = QIcon()
        icon.addFile('resources/icons/calendar.png', QSize(20, 20))
        expand_button = QPushButton()
        expand_button.setIcon(icon)
        expand_button.setFixedSize(QSize(30, 30))
        layout_date2.addWidget(expand_button)

        date_group = QGroupBox('Data')
        layout.addWidget(date_group)
        date_group.hide()
        layout.addWidget(local_group)
        local_group.hide()
        layout.addStretch()
        layout_date = QHBoxLayout()
        date_group.setLayout(layout_date)

        local_search4 = SearchList('Dias:', list(map(str, range(1, 32))))
        layout_date.addWidget(local_search4)
        local_search4.edit.textChanged.connect(lambda text: self.trigger_change(5, text))

        local_search5 = SearchList('Meses:', MONTHS)
        layout_date.addWidget(local_search5)
        local_search5.edit.textChanged.connect(lambda text: self.trigger_change(6, text))

        local_search6 = SearchList('Anos:', list(map(str, range(1900, 2020))))
        layout_date.addWidget(local_search6)
        local_search6.edit.textChanged.connect(lambda text: self.trigger_change(7, text))

        self.sync_checkbox_with_group(cb1, keywords_group)
        self.sync_checkbox_with_group(cb2, date_group)
        self.sync_checkbox_with_group(cb3, local_group)

        layout_scroll = QHBoxLayout()
        widget_scroll = QWidget()
        widget_scroll.setContentsMargins(0, 0, 0, 5)
        widget_scroll.setLayout(layout_scroll)
        layout_scroll.addWidget(widget)
        scroll_area = QScrollArea()
        scroll_area.setWidgetResizable(True)
        scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        scroll_area.setWidget(widget_scroll)

        self.addWidget(scroll_area)

    def sync_checkbox_with_group(self, cb, g):
        cb.stateChanged.connect(lambda state: g.setShown(state))

    def trigger_change(self, field, text):
        thr = QThread(self)
        thr.run = lambda: self.wait_update(field, text)
        thr.start()

    def wait_update(self, field, text):
        self.update_trigger = self.update_trigger + 1
        time.sleep(1)
        self.update_trigger = self.update_trigger - 1
        if self.update_trigger == 0:
            text_array = re.split(', |,',text)
            self.search_texts[field] = [ word for word in text_array if word != '']
            print('bora fazer a pesquisa', self.search_texts)
            # self.emit(SIGNAL('do_search()'))
            global event_handler
            event_handler.search_texts = self.search_texts
            event_handler.emit(SIGNAL('do_search()'))

    def update_tags(self, all_tags, list_type):
        print(f'updatin {list_type} words', all_tags)
        if list_type == 'people':
            self.people = all_tags
        elif list_type == 'objects':
            self.objects = all_tags
            self.object_completer.deleteLater()
            self.object_completer = TagsCompleter(self.object_input, all_tags)
            self.object_input.clicked.connect(lambda: self.object_completer.complete())
            self.object_completer.setCaseSensitivity(Qt.CaseInsensitive)
            QObject.connect(self.object_input,
                            SIGNAL('text_changed(PyQt_PyObject, PyQt_PyObject)'), self.object_completer.update)
            QObject.connect(self.object_completer, SIGNAL('activated(QString)'), self.object_input.complete_text)
            self.object_completer.setWidget(self.object_input)


def do_search(full_text):
    print('searchiinnnnnn', full_text)
    event_handler.emit_signal('do_search(QString)', full_text)
