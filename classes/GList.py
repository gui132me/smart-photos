from PyQt4.QtCore import QSize, Qt, SIGNAL
from PyQt4.QtGui import QWidget, QLineEdit, QHBoxLayout, QIcon, QPushButton, QCursor, QLabel, QVBoxLayout

from classes.QEditabledLabel import GEditableLabel


class GList(QWidget):
    def __init__(self, title):
        super(GList, self).__init__()

        # Layout
        layout = QHBoxLayout()
        layout.setContentsMargins(0, 20, 0, 0)
        widget = QWidget()
        widget.setLayout(layout)

        self.title = QLabel(title)
        self.title.setFixedWidth(335)
        self.title.setStyleSheet('font-weight: bold')
        layout.addWidget(self.title)

        icon = QIcon()
        icon.addFile('resources/icons/plus.png', QSize(20, 20))
        self.add_button = QPushButton()
        self.add_button.setIcon(icon)
        self.add_button.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.add_button)

        layout.addStretch()

        self.v_layout = QVBoxLayout()
        self.v_layout.addWidget(widget)
        self.v_layout.setMargin(0)
        self.setLayout(self.v_layout)

        self.edit = None

        # Functionality
        self.itens = []
        self.saved_text = ''
        self.add_button.clicked.connect(lambda event: self.create_edit())

    def clear(self):
        for item in self.itens:
            try:
                item.deleteLater()
            except RuntimeError:
                # widget has already sent to garbage
                pass
        self.itens = []

    def create_edit(self):
        self.edit = GEditableLabel()
        self.edit.setText('')
        self.edit.toggle_editing()
        self.edit.setStyleSheet('QLineEdit{ margin-left: 23px}')
        self.v_layout.insertWidget(1, self.edit)

        self.edit.toggle_editing = self.confirm_edit
        self.edit.cancel_editing = self.delete_editor
        self.add_button.setEnabled(False)

    def confirm_edit(self):
        text = self.edit.edit.text()
        self.delete_editor()
        self.emit(SIGNAL('added(QString)'), text)
        self.add(text)

    def delete_editor(self):
        self.edit.deleteLater()
        self.add_button.setEnabled(True)

    def add(self, text):
        layout = QHBoxLayout()
        layout.setMargin(0)
        widget = QWidget()
        widget.setLayout(layout)
        label = QLabel(text)
        label.setStyleSheet("QLabel {font: 15pt Comic Sans MS; margin-left: 20px}")
        label.setFixedWidth(335)
        layout.addWidget(label)
        icon = QIcon()
        icon.addFile('resources/icons/garbage.png', QSize(20, 20))
        button = QPushButton()
        button.setIcon(icon)
        button.setContentsMargins(0, 0, 0, 0)
        button.clicked.connect(lambda event: self.emit(SIGNAL('removed(QString)'), label.text()))
        button.clicked.connect(lambda event: widget.deleteLater())
        layout.addWidget(button)
        layout.addStretch()
        self.itens.append(widget)
        self.v_layout.addWidget(widget)
