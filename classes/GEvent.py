from PyQt4.QtCore import QObject, SIGNAL


class GEvent(QObject):
    def __init__(self):
        super(QObject, self).__init__()
        self.signals = []

    def emit_signal(self, event_name, param):
        self.emit(SIGNAL(event_name), param)