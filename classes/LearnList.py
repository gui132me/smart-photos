import pickle

from PyQt4.QtGui import QVBoxLayout, QWidget, QScrollArea, QLabel, QPushButton, QInputDialog, QCheckBox, QLineEdit, \
    QHBoxLayout
from PyQt4.QtCore import Qt, SIGNAL

import classification_service
from classes.PeopleList import small_button

folders_widgets_dict = {}
learn_layout = None
event_handler = None


def create_learn_list(main_window_event_handler):
    global event_handler
    global learn_layout
    event_handler = main_window_event_handler

    classes_file = open('resources/classes.pckl', 'rb')
    classes = pickle.load(classes_file)
    classes_file.close()

    folders = QLabel('Pastas e Nomes')
    folders.setStyleSheet("QLabel{font: bold; font-size: 18px;}")
    learn_layout = QVBoxLayout()
    learn_layout.addWidget(folders, alignment=Qt.AlignCenter)

    for (model_name, class_folder_path) in classes:
        model_folder = QCheckBox(class_folder_path)
        model_folder.setStyleSheet("QCheckBox{margin-top: 20px;}")
        model_folder.setChecked(Qt.Checked)
        learn_layout.addWidget(model_folder, alignment=Qt.AlignLeft)

        name_and_view = QHBoxLayout()
        name_and_view.addStretch()
        model_name_editor = QLineEdit(model_name)
        name_and_view.addWidget(model_name_editor, alignment=Qt.AlignCenter)
        view_button = small_button('eye')
        link_view_button(view_button, model_name)
        name_and_view.addWidget(view_button, alignment=Qt.AlignCenter)
        name_and_view.addStretch()
        name_and_view_widget = QWidget()
        name_and_view_widget.setLayout(name_and_view)
        learn_layout.addWidget(name_and_view_widget)
        # folders_widgets_dict[class_folder_path] = (model_name, model_folder)

        model_folder.stateChanged.connect(name_and_view_widget.setShown)

    learn_layout.addStretch()

    train_button = QPushButton('Treinar')
    learn_layout.addWidget(train_button)
    train_button.clicked.connect(lambda e: get_selected_folders())

    widd = QWidget()
    widd.connect(event_handler, SIGNAL('open_folder(QString)'), add_to_list)
    widd.connect(event_handler, SIGNAL('close_folder(QString)'), remove_from_list)
    widd.setLayout(learn_layout)
    scrolarea = QScrollArea()
    scrolarea.setWidget(widd)
    scrolarea.setWidgetResizable(True)
    scrolarea.horizontalScrollBar().setEnabled(False)
    return scrolarea


def add_to_list(class_folder_path):
    if class_folder_path not in folders_widgets_dict:
        model_name = class_folder_path.split('/').pop().split('\\').pop()
        model_folder = QCheckBox(class_folder_path)
        model_folder.setStyleSheet("QCheckBox{margin-top: 20px;}")
        learn_layout.insertWidget(learn_layout.count()-2, model_folder, alignment=Qt.AlignLeft)
        model_name = QLineEdit(model_name)
        learn_layout.insertWidget(learn_layout.count()-2, model_name, alignment=Qt.AlignCenter)
        model_folder.stateChanged.connect(model_name.setShown)
        model_name.hide()


def remove_from_list(class_folder_path):
    (model_name, model_folder) = folders_widgets_dict[class_folder_path]
    if not model_folder.isChecked():
        model_folder.deleteLater()
        model_name.deleteLater()
        del folders_widgets_dict[class_folder_path]


def get_selected_folders():
    learn_layout.count()
    classes = []
    for i in range(1, learn_layout.count()-2, 2):
        checkbox = learn_layout.itemAt(i).widget()
        if checkbox and checkbox.isChecked():
            model_name = learn_layout.itemAt(i+1).widget().text()
            class_folder_path = checkbox.text()
            classes.append((model_name, class_folder_path))
    classification_service.train(classes)


def link_view_button(view_button, class_name):
    global event_handler
    view_button.clicked.connect(lambda event: event_handler.emit_signal('view_class(QString)', class_name))