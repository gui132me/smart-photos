import os
import shutil

from PyQt4.QtGui import QVBoxLayout, QWidget, QScrollArea, QLabel, QPixmap, QPushButton, QInputDialog, QHBoxLayout, \
    QIcon, QMessageBox
from PyQt4.QtCore import Qt, SIGNAL, QSize

import face_rec
import people_service
from classes.GImage import GImage
from classes.QEditabledLabel import GEditableLabel

event_handler = None
people_layout = None


def giveup_person():
    people_layout.add_button.show()
    people_layout.label.deleteLater()
    people_layout.photo.deleteLater()
    people_layout.photo2.deleteLater()


def giveup_existing_person(edit_widget, widgets, widgets2):
    edit_widget.edit_layout.photo.disconnect(event_handler, SIGNAL('image_selected(QString)'),
                                             edit_widget.edit_layout.photo.changing_function)
    # widgets2.hide()
    # edit_widget.edit_layout.deleteLater()
    for i in reversed(range(edit_widget.edit_layout.count())):
        edit_widget.edit_layout.itemAt(i).widget().setParent(None)
        # edit_widget.edit_layout.itemAt(i).widget().deleteLater()
    # empty_layout = QHBoxLayout()
    # empty_layout.deleteLater()
    # lbl = QLabel('tnc kkk')
    # empty_layout.addWidget(lbl)
    # edit_widget.setLayout(empty_layout)
    edit_widget.setParent(None)
    edit_widget.deleteLater()
    for w in widgets:
        w.show()


def update_person(person, new_name, current_image, widgets, edit_widget):
    h_w = widgets[0]
    image_label = widgets[2]
    name_label = widgets[1]
    if new_name != person.name:
        print('will change name')
        ans = rename_dialog(person, new_name)
        if ans:
            name_label.setText(person.name)
    if current_image and ans:
        os.remove(f'faces/{person.id}.jpg')
        shutil.copy(current_image, f'faces/{person.id}.jpg')
        p = QPixmap(f'faces/{person.id}.jpg').scaledToHeight(200, Qt.SmoothTransformation)
        people_service.change_face(f'./faces/{person.id}.jpg', person)
        image_label.setPixmap(p)

        print('will change image')

    # for i in reversed(range(edit_widget.edit_layout.count())):
    #     edit_widget.edit_layout.itemAt(i).widget().setParent(None)

    edit_widget.setParent(None)
    edit_widget.deleteLater()

    # for w in widgets:
    #     w.show()
    people_layout.insertWidget(people.index(person) + 1, person_item(person))


def keep_created_person_and_clear(new_person):
    people_layout.add_button.show()
    people_layout.label.deleteLater()
    people_layout.photo.deleteLater()
    people_layout.photo2.deleteLater()

    person_label = QLabel(new_person.name)
    pixmap = QPixmap(f'faces/{new_person.id}.jpg').scaledToHeight(200, Qt.SmoothTransformation)
    label = QLabel()
    label.setPixmap(pixmap)
    person_label.setStyleSheet('margin-bottom: 30')
    h_layout = QHBoxLayout()
    h_widget = QWidget()
    h_widget.setLayout(h_layout)
    h_layout.addWidget(label)
    buttons = QWidget()
    buttons_l = QVBoxLayout()
    edit_button = small_button('edit')
    delete_button = small_button('garbage')
    # buttons_l.addWidget(edit_button)
    buttons_l.addWidget(delete_button)
    buttons.setLayout(buttons_l)
    h_layout.addWidget(buttons, alignment=Qt.AlignTop)
    h_layout.addStretch()
    connect_del_button(delete_button, new_person, [h_widget, person_label])
    people_layout.insertWidget(1, h_widget, alignment=Qt.AlignCenter)
    people_layout.insertWidget(2, person_label, alignment=Qt.AlignCenter)


def create_person(txt, img, editing_widget):
    editing_widget.edit_layout.photo.disconnect(event_handler, SIGNAL('image_selected(QString)'),
                                             editing_widget.edit_layout.photo.changing_function)
    # print('new person', txt)
    # print('image', img)
    # extension = img.split('.').pop()
    # print(f'{txt}.{extension}')
    # shutil.copy(img, f'faces/{txt}.{extension}')
    new_person = people_service.add(img, txt)
    people.insert(0, new_person)
    # keep_created_person_and_clear(new_person)
    item = person_item(new_person)
    editing_widget.deleteLater()
    editing_widget.setParent(None)
    people_layout.add_button.show()
    people_layout.insertWidget(1, item)

    # face_rec.add_encoding(f'{txt}.{extension}')


def changing_photo(image_path, label1, label2):
    global current_image
    current_image = image_path
    try:
        p = QPixmap(image_path).scaledToHeight(200, Qt.SmoothTransformation)
        label1.setPixmap(p)
        pessoas, new_detection = people_service.find(image_path)
        if len(pessoas) == 1:
            label2.setText('Uma pessoa encontrada')
        elif len(pessoas) > 4:
            label2.setText('Várias pessoas encontradas')
        elif len(pessoas) != 0:
            quant = ['Duas', 'Três', 'Quatro']
            label2.setText(quant[len(pessoas) - 2] + ' pessoas encontradas')
        label2.show()
    except RuntimeError:
        print('wrapped C/C++ object of type QLabel has been deleted')


def rename_dialog(person, new_name):
    if person.name == new_name:
        return False
    qm = QMessageBox()
    qm.setWindowIcon(QIcon('resources/icons/logo_unesp.png'))
    qm.setWindowTitle(f'{person.name} → {new_name}')
    images = people_service.usage(person)
    if len(images):
        if len(images) > 1:
            txt = f'Esta pessoa foi encontrada em {len(images)} imagens. Deseja renomear o nome nas imagens também?'
        else:
            txt = f'Esta pessoa foi encontrada em {len(images)} imagem. Deseja renomear o nome nesta imagem também?'
        qm.setText(txt)
        qm.addButton(QPushButton('Apenas renomear'), QMessageBox.NoRole)
        qm.addButton(QPushButton('Renomear e alterar imagens'), QMessageBox.YesRole)
        qm.addButton(QPushButton('Cancelar'), QMessageBox.RejectRole)
        ret = qm.exec_()
        if ret == 1:
            people_service.rename(person, new_name)
            old_name = person.name
            person.name = new_name
            print('renomear tuuuudo')
            for image_path in images:
                image = GImage(image_path)
                image.delete_keyword(old_name)
                image.add_keyword(new_name)
                image.deleteLater()
        elif ret == 0:
            people_service.rename(person, new_name)
        else:
            print('cancelou', ret)
    else:
        qm.setText(f'Tem certeza que quer renomear?')
        qm.addButton(QPushButton('Remover'), QMessageBox.NoRole)
        qm.addButton(QPushButton('Cancelar'), QMessageBox.RejectRole)
        ret = qm.exec_()
        if ret == 0:
            print('remover simples')
            people_service.rename(person, new_name)
        else:
            print('cancelou', ret)
    return True


def add_person():
    edit_widget = edit_item()
    people_layout.insertWidget(1, edit_widget)
    people_layout.add_button.hide()


def add_perso_oldn():

    people_layout.label = QLabel('Selecione uma foto ao lado')
    people_layout.label.setStyleSheet('margin: 9')
    people_layout.insertWidget(1, people_layout.label)
    people_layout.photo = QLabel('Nenhuma foto selecionada')
    people_layout.photo2 = QLabel('Nenhuma pessoa encontrada')
    people_layout.photo.changing_function = lambda event: changing_photo(event, people_layout.photo,
                                                                         people_layout.photo2)
    people_layout.photo.signal = SIGNAL('image_selected(QString)')
    people_layout.photo.connect(event_handler, SIGNAL('image_selected(QString)'), people_layout.photo.changing_function)
    people_layout.photo.setAlignment(Qt.AlignHCenter)
    people_layout.insertWidget(2, people_layout.photo)
    people_layout.photo2.setAlignment(Qt.AlignHCenter)
    people_layout.photo2.hide()
    people_layout.insertWidget(3, people_layout.photo2)
    people_layout.input_dialog = QInputDialog()

    people_layout.insertWidget(4, people_layout.input_dialog)
    people_layout.input_dialog.accepted.connect(
        lambda: create_person(people_layout.input_dialog.textValue(), event_handler.current_image))
    people_layout.input_dialog.rejected.connect(lambda: giveup_person())
    people_layout.input_dialog.setLabelText('Nome da pessoa:')
    people_layout.input_dialog.setCancelButtonText('&Cancelar')
    people_layout.input_dialog.setStyleSheet('QInputDialog { margin-right: 10 } ')

    people_layout.add_button.hide()


def connect_edit_ok(input_dialog):
    input_dialog.accepted.connect(
        lambda: create_person(people_layout.input_dialog.textValue(), event_handler.current_image))

def create_people_list(main_event_handler):
    global event_handler
    event_handler = main_event_handler

    global people_layout
    people_layout = QVBoxLayout()

    global button_add
    button_add = QPushButton()
    button_add.setFixedWidth(320)
    button_add.setText('Adicionar')
    button_add.clicked.connect(add_person)
    people_layout.add_button = button_add
    people_layout.addWidget(button_add)

    global people
    people = people_service.get_saved_people()

    for person in people:
        person_widget = person_item(person)
        people_layout.addWidget(person_widget)

    people_layout.addStretch()
    widd = QWidget()
    widd.setLayout(people_layout)
    scrolarea = QScrollArea()
    scrolarea.setWidget(widd)
    scrolarea.setWidgetResizable(True)
    scrolarea.horizontalScrollBar().setEnabled(False)

    return scrolarea


def edit_item(person=None):
    event_handler.current_image = None
    edit_widget = QWidget()
    edit_layout = QVBoxLayout()
    edit_widget.edit_layout = edit_layout
    edit_layout.label = QLabel('Selecione uma foto ao lado')
    edit_layout.label.setStyleSheet('margin: 9')
    edit_layout.addWidget(edit_layout.label)
    edit_layout.photo = QLabel('Nenhuma foto selecionada')
    edit_layout.photo2 = QLabel('Nenhuma pessoa encontrada')
    edit_layout.photo.changing_function = lambda event: changing_photo(event, edit_layout.photo, edit_layout.photo2)
    edit_layout.photo.connect(event_handler, SIGNAL('image_selected(QString)'), edit_layout.photo.changing_function)
    edit_layout.photo.setAlignment(Qt.AlignHCenter)
    edit_layout.addWidget(edit_layout.photo)
    edit_layout.photo2.setAlignment(Qt.AlignHCenter)
    edit_layout.photo2.hide()
    edit_layout.addWidget(edit_layout.photo2)
    edit_layout.input_dialog = QInputDialog()
    edit_layout.addWidget(edit_layout.input_dialog)
    # edit_layout.input_dialog.rejected.connect(lambda: giveup_existing_person(edit_widget, widgets, widgets2))
    edit_layout.input_dialog.setLabelText('Nome da pessoa:')
    if person:
        pixmap = QPixmap(f'./faces/{person.id}.jpg').scaledToHeight(200, Qt.SmoothTransformation)
        edit_layout.photo.setPixmap(pixmap)
        edit_layout.input_dialog.setTextValue(person.name)
        edit_layout.input_dialog.accepted.connect(
            lambda: create_person(edit_layout.input_dialog.textValue(), event_handler.current_image, edit_widget))
    else:
        edit_layout.input_dialog.accepted.connect(
            lambda: create_person(edit_layout.input_dialog.textValue(), event_handler.current_image, edit_widget))
    edit_layout.input_dialog.rejected.connect(
        lambda: giveup_editing(edit_widget, person))
    edit_layout.input_dialog.setCancelButtonText('&Cancelar')
    edit_layout.input_dialog.setStyleSheet('QInputDialog { margin-right: 10 } ')

    edit_widget.setFixedWidth(330)
    edit_widget.setLayout(edit_layout)
    return edit_widget


def giveup_editing(edit_widget, person=None):
    edit_widget.edit_layout.photo.disconnect(event_handler, SIGNAL('image_selected(QString)'),
                                             edit_widget.edit_layout.photo.changing_function)
    people_layout.add_button.show()
    edit_widget.setParent(None)
    edit_widget.deleteLater()
    if person:
        w = person_item(person)
        people_layout.insertWidget(people.index(person)+1, w)


def person_item(person=None):
    item_layout = QVBoxLayout()
    person_label = QLabel(person.name)
    pixmap = QPixmap(f'faces/{person.id}.jpg').scaledToHeight(200, Qt.SmoothTransformation)
    label = QLabel()
    label.setPixmap(pixmap)
    person_label.setStyleSheet('margin-bottom: 30')
    h_layout = QHBoxLayout()
    h_widget = QWidget()
    h_widget.setLayout(h_layout)
    h_layout.addWidget(label)
    buttons = QWidget()
    buttons_l = QVBoxLayout()
    edit_button = small_button('edit')
    delete_button = small_button('garbage')
    view_button = small_button('eye')
    buttons_l.addWidget(view_button)
    buttons_l.addWidget(edit_button)
    buttons_l.addWidget(delete_button)
    buttons.setLayout(buttons_l)
    h_layout.addWidget(buttons, alignment=Qt.AlignTop)
    h_layout.addStretch()
    connect_view_button(view_button, person)
    connect_del_button(delete_button, person, [h_widget, person_label])
    edit_widget = QWidget()
    edit_widget.edit_layout = QVBoxLayout()
    edit_widget.hide()
    connect_edit_button(edit_button, person, [h_widget, person_label, label], edit_widget)
    item_layout.addWidget(h_widget, alignment=Qt.AlignCenter)
    item_layout.addWidget(person_label, alignment=Qt.AlignCenter)
    item_layout.addWidget(edit_widget, alignment=Qt.AlignCenter)
    item_widget = QWidget()
    item_widget.setLayout(item_layout)
    return item_widget


def view_person(person):
    event_handler.emit(SIGNAL('view_person(int, QString)'), person.id, person.name)


def connect_view_button(delete_button, person):
    delete_button.clicked.connect(lambda event: view_person(person))


def connect_del_button(delete_button, person, widgets):
    delete_button.clicked.connect(lambda event: delete_dialog(person, widgets))


def connect_edit_button(edit_button, person, widgets, widgets2):
    edit_button.clicked.connect(lambda event: edit_widgets(person, widgets, widgets2))


def edit_widgets(person, widgets, widgets2):
    event_handler.current_image = None
    edit_widget = widgets2
    edit_layout = edit_widget.edit_layout
    edit_layout.label = QLabel('Selecione uma foto ao lado')
    edit_layout.label.setStyleSheet('margin: 9')
    edit_layout.addWidget(edit_layout.label)
    pixmap = QPixmap(f'./faces/{person.id}.jpg').scaledToHeight(200, Qt.SmoothTransformation)
    edit_layout.photo = QLabel('Nenhuma foto selecionada')
    edit_layout.photo2 = QLabel('Nenhuma pessoa encontrada')
    edit_layout.photo.setPixmap(pixmap)
    edit_layout.photo.changing_function = lambda event: changing_photo(event, edit_layout.photo, edit_layout.photo2)
    edit_layout.photo.connect(event_handler, SIGNAL('image_selected(QString)'), edit_layout.photo.changing_function)
    edit_layout.photo.setAlignment(Qt.AlignHCenter)
    edit_layout.addWidget(edit_layout.photo)
    edit_layout.photo2.setAlignment(Qt.AlignHCenter)
    edit_layout.photo2.hide()
    edit_layout.addWidget(edit_layout.photo2)
    edit_layout.input_dialog = QInputDialog()
    edit_layout.addWidget(edit_layout.input_dialog)
    edit_layout.input_dialog.accepted.connect(
        lambda: update_person(person, edit_layout.input_dialog.textValue(), event_handler.current_image, widgets,
                              edit_widget))
    edit_layout.input_dialog.rejected.connect(lambda: giveup_existing_person(edit_widget, widgets, widgets2))
    edit_layout.input_dialog.setLabelText('Nome da pessoa:')
    edit_layout.input_dialog.setTextValue(person.name)
    edit_layout.input_dialog.setCancelButtonText('&Cancelar')
    edit_layout.input_dialog.setStyleSheet('QInputDialog { margin-right: 10 } ')

    edit_widget.setFixedWidth(330)
    edit_widget.setLayout(edit_layout)
    edit_widget.show()

    print('editing person', person.name)
    for w in widgets:
        w.hide()


def small_button(name):
    icon = QIcon()
    icon.addFile('resources/icons/'+name+'.png', QSize(20, 20))
    button = QPushButton()
    button.setIcon(icon)
    button.setContentsMargins(0, 0, 0, 0)
    return button


def delete_widgets(widgets):
    for w in widgets:
        w.deleteLater()


# ask if user is sure
def delete_dialog(person, widgets):
    qm = QMessageBox()
    qm.setWindowIcon(QIcon('resources/icons/logo_unesp.png'))
    qm.setWindowTitle(person.name)
    images = people_service.usage(person)
    if len(images):
        if len(images) > 1:
            txt = f'Esta pessoa foi encontrada em {len(images)} imagens. Deseja remove-la dessas imagens também?'
        else:
            txt = f'Esta pessoa foi encontrada em {len(images)} imagem. Deseja remove-la dessa imagem também?'
        qm.setText(txt)
        qm.addButton(QPushButton('Apenas remover da lista'), QMessageBox.NoRole)
        qm.addButton(QPushButton('Remover das imagens também'), QMessageBox.YesRole)
        qm.addButton(QPushButton('Cancelar'), QMessageBox.RejectRole)
        ret = qm.exec_()
        if ret == 1:
            print('remover tuuuudo')
            for image in images:
                people_service.delete(person, image)
            people_service.delete(person)
            delete_widgets(widgets)
        elif ret == 0:
            print('remover simples')
            people_service.delete(person)
            delete_widgets(widgets)
        else:
            print('cancelou', ret)
    else:
        qm.setText(f'Remover esta pessoa?')
        qm.addButton(QPushButton('Remover'), QMessageBox.NoRole)
        qm.addButton(QPushButton('Cancelar'), QMessageBox.RejectRole)
        ret = qm.exec_()
        if ret == 0:
            print('remover simples')
            people_service.delete(person)
            delete_widgets(widgets)
        else:
            print('cancelou', ret)


