from PyQt4.QtGui import QToolBar
from classes.SystemExplorer import create_system_explorer
from classes.PeopleList import create_people_list
from classes.ObjectsList import create_objects_list
from classes.LearnList import create_learn_list


event_handler = None


class GToolBar(QToolBar):
    def __init__(self, main_window_event_handler):
        super(GToolBar, self).__init__()
        global event_handler
        event_handler = main_window_event_handler
        self.current_tool = None
        self.setMovable(False)
        self.tools = dict()
        self.hide()

    def change_toolbar(self, tool_name):
        if tool_name == self.current_tool and not self.isHidden():
            self.hide()
        else:
            if self.current_tool:
                self.removeAction(self.tools[self.current_tool])
            self.current_tool = tool_name
            if tool_name in self.tools:
                self.addAction(self.tools[tool_name])
            else:
                self.tools[tool_name] = self.addWidget(create_widget(tool_name))
            self.show()

    def load_tool(self, tool_name):
        ready_action = self.addWidget(create_widget(tool_name))
        self.removeAction(ready_action)
        self.tools[tool_name] = ready_action


# map all events
def create_widget(tool):
    widget = None
    if tool == 'folder':
        widget = create_system_explorer(event_handler)
    if tool == 'people':
        widget = create_people_list(event_handler)
    if tool == 'objects':
        widget = create_objects_list(event_handler)
    if tool == 'learn':
        widget = create_learn_list(event_handler)
    return widget
