from PyQt4.QtCore import Qt, SIGNAL, QObject
from PyQt4.QtGui import QMainWindow, QIcon, QScrollArea
from classes.ButtonsToolBar import ButtonsToolBar
from classes.GEvent import GEvent
from classes.GTabWidget import GTabWidget
from classes.GToolBar import GToolBar
from classes.PropertiesBar import PropertiesBar
from classes.SearchBar import SearchBar


class GMainWindow(QMainWindow):
    def __init__(self, event_handler):
        super(GMainWindow, self).__init__()

        self.setWindowTitle("Gerenciamento de Imagens")
        self.setWindowIcon(QIcon("resources/icons/logo_unesp.png"))
        self.selected_objects = []
        self.event_handler = event_handler
        self.search_bar = SearchBar(self.event_handler, self.selected_objects)
        self.addToolBar(Qt.TopToolBarArea, self.search_bar)
        self.toolbar_buttons = ButtonsToolBar(self.search_bar)
        self.addToolBar(Qt.LeftToolBarArea, self.toolbar_buttons)
        self.toolbar = GToolBar(self.event_handler)
        self.toolbar.connect(self.toolbar_buttons, SIGNAL('folder_selected(QString)'),
                             lambda event: self.toolbar.change_toolbar(event))
        self.addToolBarBreak(Qt.LeftToolBarArea)
        self.addToolBar(Qt.LeftToolBarArea, self.toolbar)
        self.properties_bar = PropertiesBar(self.event_handler, self.selected_objects)
        self.addToolBar(Qt.RightToolBarArea, self.properties_bar)
        self.receive_signals()
        self.central_widget = GTabWidget(self.event_handler)
        self.setCentralWidget(self.central_widget)
        self.toolbar.load_tool('objects')
        self.toolbar.load_tool('people')
        self.toolbar.load_tool('learn')
        self.showMaximized()

    def receive_signals(self):
        QObject.connect(self.event_handler, SIGNAL('open_image(QString)'), lambda event: self.open_image(event))
        QObject.connect(self.event_handler, SIGNAL('open_folder(QString)'),
                        lambda event: self.central_widget.open_folder(event))
        QObject.connect(self.event_handler, SIGNAL('objects_changed(QStringList)'),
                        lambda event: self.update_objects_list(event))
        # QObject.connect(self.event_handler, SIGNAL('people_changed(QStringList)'),
        #                 lambda event: self.update_people_list(event))

    def update_people_list(self, people_list):
        print('people updated')
        self.search_bar.update_tags(people_list, 'people')

    def update_objects_list(self, object_list):
        print('objetcs updated')
        self.selected_objects = object_list
        self.properties_bar.selected_objects = object_list
        # self.search_bar.update_tags(object_list, 'objects')

    def open_image(self, image_path):
        self.properties_bar.open_image(image_path)

