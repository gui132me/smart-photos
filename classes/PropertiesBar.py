import os
import time

import exifread
from PyQt4.QtCore import Qt, QObject, SIGNAL, QSize, QPoint
from PyQt4.QtGui import QToolBar, QPixmap, QLabel, QWidget, QScrollArea, QVBoxLayout, QCursor, QLineEdit, QMainWindow, \
    QGraphicsScene, QGraphicsView, QCalendarWidget, QPushButton, QIcon
from iptcinfo3 import IPTCInfo
import reverse_geocoder as rg


# import classification
import classification_service
import face_rec
import object_detection
import objects_service
import people_service
from classes.GDateChanger import GDateChanger
from classes.GImage import GImage
from classes.GList import GList
from classes.GLocalChanger import GLocalChanger
from classes.QEditabledLabel import GEditableLabel

event_handler = None


class PropertiesBar(QToolBar):
    def mouseMoveEvent(self):
        pass

    def __init__(self, main_window_event_handler, selected_objects):
        super(PropertiesBar, self).__init__()
        self.setMouseTracking(False)
        # self.mouseMoveEvent()
        self.setCursor(Qt.PointingHandCursor)
        global event_handler
        event_handler = main_window_event_handler

        self.image = None
        self.full_screen_window = None

        self.setStyleSheet("QLabel {font: 15pt Comic Sans MS}")
        self.setMovable(False)
        self.hide()
        self.selected_objects = selected_objects

        scroll_widget = QWidget()
        scroll_widget.setStyleSheet("QWidget{ background-color: white}")

        self.scroll_layout = QVBoxLayout()
        self.scroll_layout.setContentsMargins(5, 5, 0, 5)
        scroll_widget.setLayout(self.scroll_layout)
        scrollarea = QScrollArea()
        scrollarea.setMinimumWidth(400)
        scrollarea.setWidget(scroll_widget)
        scrollarea.setWidgetResizable(True)
        scrollarea.horizontalScrollBar().setEnabled(False)
        self.scroll_layout.setStretchFactor(scroll_widget, 1)
        self.addWidget(scrollarea)

        self.current_image = None
        self.name_label = GEditableLabel()
        QObject.connect(self.name_label, SIGNAL('image_renamed(QString)'), lambda event: self.rename_image(event))
        self.scroll_layout.addWidget(self.name_label)
        self.extension_label = QLabel()
        self.extension_label.setStyleSheet('font: 10pt Comic Sans MS; margin-left: 20px;')
        self.scroll_layout.addWidget(self.extension_label)
        self.creation_label = QLabel()
        self.creation_label.setStyleSheet('font: 10pt Comic Sans MS; margin-left: 20px;')
        self.scroll_layout.addWidget(self.creation_label)
        self.modfication_label = QLabel()
        self.modfication_label.setStyleSheet('font: 10pt Comic Sans MS; margin-left: 20px;')
        self.scroll_layout.addWidget(self.modfication_label)
        self.image_label = QLabel()
        self.image_label.setMouseTracking(True)
        self.image_label.setCursor(Qt.PointingHandCursor)
        self.image_label.mouseReleaseEvent = lambda event: self.full_screen()
        self.scroll_layout.addWidget(self.image_label, alignment=Qt.AlignHCenter)

        icon = QIcon()
        icon.addFile('resources/icons/expand.png', QSize(20, 20))
        self.image_expand = QPushButton(self.image_label)
        self.image_expand.setIcon(icon)
        self.image_expand.setFixedSize(QSize(30, 30))
        self.image_expand.setMouseTracking(True)
        self.image_expand.setCursor(Qt.PointingHandCursor)
        self.image_expand.mouseReleaseEvent = lambda event: self.full_screen()
        self.image_expand.show()
        # self.scroll_layout.addWidget(self.image_expand)

        self.text_date_label = QLabel('Data da foto:')
        self.text_date_label.setStyleSheet('font-weight: bold')
        self.scroll_layout.addWidget(self.text_date_label)
        self.date_label = GDateChanger()
        QObject.connect(self.date_label, SIGNAL('date_changed(QDate)'), lambda event: self.change_date(event))
        self.date_label.setStyleSheet("QLabel {font: 15pt Comic Sans MS; margin-left: 20px}")
        self.scroll_layout.addWidget(self.date_label)
        self.text_local_label = QLabel('Local:')
        self.text_local_label.setStyleSheet('QLabel {font-weight: bold; margin: 20px 0 0 -5px}')
        self.scroll_layout.addWidget(self.text_local_label)
        self.local_label = GLocalChanger()
        self.local_label.setStyleSheet("QLineEdit {margin-left: 20px}")
        QObject.connect(self.local_label, SIGNAL('coord_changed(QString, PyQt_PyObject, PyQt_PyObject)'),
                        self.change_gps)
        self.scroll_layout.addWidget(self.local_label)
        self.text_people_label = GList('Pessoas:')
        self.text_people_label.setStyleSheet('font-weight: bold')
        QObject.connect(self.text_people_label, SIGNAL('removed(QString)'),
                        self.delete_keyword)
        QObject.connect(self.text_people_label, SIGNAL('added(QString)'),
                        self.add_person)
        self.scroll_layout.addWidget(self.text_people_label)
        self.people_labels = []
        self.text_object_label = GList('Objetos:')
        self.text_object_label.setStyleSheet('font-weight: bold')
        QObject.connect(self.text_object_label, SIGNAL('removed(QString)'),
                        self.delete_keyword)
        QObject.connect(self.text_object_label, SIGNAL('added(QString)'),
                        self.add_keyword)
        self.scroll_layout.addWidget(self.text_object_label)
        self.object_labels = []

        icon = QIcon()
        icon.addFile('resources/icons/cancel.png', QSize(20, 20))
        close_button = QPushButton()
        # close_button.setStyleSheet("margin: 60")
        close_button.setIcon(icon)
        close_button.setFixedSize(QSize(30, 30))
        close_button.setCursor(Qt.PointingHandCursor)
        close_button.mouseReleaseEvent = lambda e: self.hide()
        self.scroll_layout.addStretch()
        self.scroll_layout.addWidget(close_button, alignment=Qt.AlignRight | Qt.AlignBottom)

    def add_person(self, person):
        people_service.add_person(person)
        self.add_keyword(person)

    def add_keyword(self, word):
        self.image.add_keyword(word)

    def delete_keyword(self, word):
        self.image.delete_keyword(word)
        event_handler.emit_signal('keyword_removed(QStringList)', [self.image.path(), word])

    def change_date(self, date):
        self.image.change_date(date)

    def change_gps(self, txt, lat, lon):
        self.image.change_gps(txt, lat, lon)

    def full_screen(self):
        self.full_screen_window = QMainWindow()
        pixmap = self.image.pixmap()
        label = QLabel()
        label.setAlignment(Qt.AlignCenter)
        self.full_screen_window.setWindowTitle(self.current_image.split('/').pop().split('\\').pop())
        self.full_screen_window.setWindowIcon(QIcon("resources/icons/logo_unesp.png"))
        self.full_screen_window.setCentralWidget(label)
        self.full_screen_window.showMaximized()
        pixmap = pixmap.scaledToWidth(self.full_screen_window.width(),
                                      Qt.SmoothTransformation).scaledToHeight(self.full_screen_window.height(),
                                                                              Qt.SmoothTransformation)

        label.setPixmap(pixmap)

    def rename_image(self, text):
        self.image.rename(text)

    def open_image(self, image_path):
        
        image = GImage(image_path, event_handler)
        if self.image:
            self.image.deleteLater()
        self.image = image

        event_handler.current_image = image.path()
        event_handler.emit_signal('image_selected(QString)', image.path())

        self.current_image = image.path()

        file_name_full = image.path().split('/').pop().split('\\').pop()
        pixmap = QPixmap(image.path()).scaledToWidth(250, Qt.SmoothTransformation)\
            .scaledToHeight(250, Qt.SmoothTransformation)

        file_name = file_name_full.split('.')[0]
        self.name_label.setText(file_name)
        # self.name_label.setMaximumWidth(pixmap.width())

        extension = file_name_full.split('.').pop()
        self.extension_label.setText(f'Arquivo {extension}')

        data = time.localtime(os.path.getctime(image.path()))
        dia = data.tm_mday
        mes = data.tm_mon
        ano = data.tm_year
        self.creation_label.setText(f'Criado em: {dia}/{mes}/{ano}')

        data = time.localtime(os.path.getmtime(image.path()))
        dia = data.tm_mday
        mes = data.tm_mon
        ano = data.tm_year
        self.modfication_label.setText(f'Modificado em: {dia}/{mes}/{ano}')

        self.image_label.setPixmap(pixmap)
        self.image_label.setMouseTracking(True)
        self.image_label.setCursor(Qt.PointingHandCursor)

        self.image_expand.move(pixmap.width()-40, 10)
        self.image_expand.setStyleSheet("background-color: rgba(255, 255, 255, 0.7);")

        self.date_label.reset()
        if self.image.has_date():
            self.date_label.setDate(self.image.date())
        else:
            self.date_label.setText('Não Definida')

        # gps_keys = ['GPS GPSLatitudeRef', 'GPS GPSLatitude', 'GPS GPSLongitudeRef', 'GPS GPSLongitude']
        # tags = self.image.exif_tags()

        self.local_label.reset()
        if self.image.has_local():
            self.local_label.setLocal(self.image.local())
        else:
            self.local_label.removeLocal()

        detected_people, new_detection_people = people_service.find(image_path)

        for d in detected_people:
            print('recognition -', d)

        if new_detection_people:
            image.delete_keyword('Desconhecido')
            image.add_keywords(detected_people)
        else:
            detected_people = image.people()

        self.text_people_label.clear()

        self.people_labels = []

        for pessoa in detected_people:
            # if pessoa is not 'Desconhecido':
            self.text_people_label.add(pessoa)

        detected_objects, new_detection_object = objects_service.find(image_path)
        for detected_object in detected_objects:
            print('detection -', detected_object)
        self.text_object_label.clear()

        if new_detection_object:
            current_objects = image.objects()
            detected_n_selected_obj = []
            for detected_object in detected_objects:
                if detected_object in self.selected_objects:
                    detected_n_selected_obj.append(detected_object)
            self.image.add_keywords(detected_n_selected_obj)
            for old_obj in current_objects:
                if old_obj not in detected_objects:
                    detected_objects.append(old_obj)
        else:
            detected_objects = image.objects()

        for detected_object in detected_objects:
            self.text_object_label.add(detected_object)

        self.show()
