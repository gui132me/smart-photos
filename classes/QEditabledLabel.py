from PyQt4.QtCore import QSize, Qt, SIGNAL
from PyQt4.QtGui import QWidget, QLineEdit, QHBoxLayout, QIcon, QPushButton, QCursor


class GEditableLabel(QWidget):
    def __init__(self):
        super(GEditableLabel, self).__init__()

        # Layout
        layout = QHBoxLayout()
        layout.setMargin(0)
        self.setLayout(layout)

        self.edit = QLineEdit(self)
        self.edit.setFixedWidth(335)
        self.edit.setReadOnly(True)
        self.edit.setStyleSheet("QLineEdit {font: 15pt Comic Sans MS; border: none;}")
        self.edit.setCursor(QCursor(Qt.ArrowCursor))
        # self.edit.setMouseTracking(True)
        # self.edit.cursorPositionChanged.connect(lambda event: self.edit.setCursor(QCursor(Qt.ArrowCursor)))
        layout.addWidget(self.edit)

        icon = QIcon()
        icon.addFile('resources/icons/edit.png', QSize(20, 20))
        self.button = QPushButton()
        self.button.setIcon(icon)
        self.button.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.button)

        icon = QIcon()
        icon.addFile('resources/icons/cancel.png', QSize(20, 20))
        self.cancel_button = QPushButton()
        self.cancel_button.setIcon(icon)
        self.cancel_button.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.cancel_button)
        self.cancel_button.hide()

        layout.addStretch()

        # Functionality
        self.is_editing = False
        self.saved_text = ''
        self.button.clicked.connect(lambda event: self.toggle_editing())
        self.cancel_button.clicked.connect(lambda event: self.cancel_editing())

    def setText(self, text):
        self.edit.setText(text)
        self.saved_text = text

    def toggle_editing(self):
        if self.is_editing:
            self.saved_text = self.edit.text()
            self.stop_editing()
            self.emit(SIGNAL('image_renamed(QString)'), self.saved_text)
        else:
            self.edit.setReadOnly(False)
            self.edit.setStyleSheet("QLineEdit {font: 15pt Comic Sans MS; border: 1px solid black;}")
            self.edit.setFocus(True)
            self.cancel_button.show()
            self.is_editing = True
            icon = QIcon()
            icon.addFile('resources/icons/tick.png', QSize(20, 20))
            self.button.setIcon(icon)
            self.edit.setFixedWidth(302)

    def stop_editing(self):
        self.edit.setReadOnly(True)
        self.edit.setStyleSheet("QLineEdit {font: 15pt Comic Sans MS; border: 0;}")
        self.cancel_button.hide()
        self.is_editing = False
        icon = QIcon()
        icon.addFile('resources/icons/edit.png', QSize(20, 20))
        self.button.setIcon(icon)
        self.edit.setFixedWidth(335)

    def cancel_editing(self):
        self.setText(self.saved_text)
        self.stop_editing()