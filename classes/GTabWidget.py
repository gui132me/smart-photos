import _thread
import glob
import os
import time

from PyQt4.QtCore import Qt, QObject, SIGNAL
from PyQt4.QtGui import QTabWidget, QWidget, QScrollArea, QPixmap, QLabel, QCursor, QVBoxLayout, QPushButton, \
    QHBoxLayout, QMessageBox, QIcon

import classification_service
import objects_service
import people_service
from classes.FlowLayout import FlowLayout
from classes.GImage import GImage


def normalize_path(path):
    return path.replace('\\', '/')


def is_image(path):
    path = path.lower()
    extensions = [".jpg", ".jpeg", ".png", ".gif"]
    for ext in extensions:
        if path.endswith(ext):
            return True
    return False


class GTabWidget(QTabWidget):
    def __init__(self, event_handler):
        super(GTabWidget, self).__init__()
        self.event_handler = event_handler
        self.folders = []
        self.selectable_objects = []
        self.images = []
        self.setMinimumWidth(410)
        tab1 = no_folder()
        self.addTab(tab1, "Nenhuma pasta")

        self.connect(self.event_handler, SIGNAL('objects_changed(QStringList)'), self.update_objects_list)

        self.connect(self.event_handler, SIGNAL('image_renamed(QStringList)'), self.image_rename_listener)
        self.connect(self.event_handler, SIGNAL('date_changed(QString, QDate)'), self.date_change_listener)
        self.connect(self.event_handler, SIGNAL('local_changed(QString, QString)'), self.location_listener)
        self.connect(self.event_handler, SIGNAL('keyword_added(QStringList)'), self.keyword_added)
        # self.connect(self.event_handler, SIGNAL('keyword_removed(QStringList)'), self.keyword_removed)

        self.connect(self.event_handler, SIGNAL('view_person(int, QString)'), self.view_person)
        self.connect(self.event_handler, SIGNAL('view_object(QString)'), self.view_object)
        self.connect(self.event_handler, SIGNAL('view_class(QString)'), self.view_class)

    def update_objects_list(self, objects_list):
        self.selectable_objects = objects_list

    def link_folder(self, folder_frame, path):
        folder_frame.mouseReleaseEvent = lambda event: self.event_handler.emit_signal('open_folder(QString)', path)

    def keyword_added(self, file_path, keyword):
        folder = os.path.dirname(file_path)
        try:
            folder_index = self.folders.index(folder)
            opened_images = self.images[folder_index]
            opened_images_obj = self.images[folder_index]
            print('folder is opened')
            for image in opened_images:
                if image.path() == file_path:
                    print('image found')
                    # opened_images_obj
        except ValueError:
            # folder is not opedened, image was oppened by the system explorer
            pass

    def location_listener(self, file_path, new_local):
        folder = os.path.dirname(file_path)
        try:
            folder_index = self.folders.index(folder)
            opened_images = self.images[folder_index]
            print('folder is opened')
            for image in opened_images:
                if image.path() == file_path:
                    if new_local:
                        print('deletou')
                        image.set_local(new_local)
                    else:
                        image.set_local(None)
                    print('image found')
        except ValueError:
            # folder is not opedened, image was oppened by the system explorer
            pass

    def date_change_listener(self, file_path, date):
        if date.isNull():
            print('deletou')
            folder = os.path.dirname(file_path)
            try:
                folder_index = self.folders.index(folder)
                opened_images = self.images[folder_index]
                print('folder is opened')
                for image in opened_images:
                    if image.path() == file_path:
                        image.setDate(date)
                        print('image found')
            except ValueError:
                # folder is not opedened, image was oppened by the system explorer
                pass
        else:
            print(date)
            folder = os.path.dirname(file_path)
            try:
                folder_index = self.folders.index(folder)
                opened_images = self.images[folder_index]
                print('folder is opened')
                for image in opened_images:
                    if image.path() == file_path:
                        image.setDate(date)
                        print('image found')
            except ValueError:
                # folder is not opedened, image was oppened by the system explorer
                pass

    def image_rename_listener(self, changes):
        old_name = changes[0]
        new_name = changes[1]
        folder = os.path.dirname(old_name)
        try:
            folder_index = self.folders.index(folder)
            opened_images = self.images[folder_index]
            print('folder is opened')
            for image in opened_images:
                if image.path() == old_name:
                    image.logical_rename(new_name)
                    print('image found')

            print('simple change detected', old_name, 'to', new_name)
        except ValueError:
            # folder is not opedened, image was oppened by the system explorer
            pass

    def open_image(self, image):
        image.remove_highlight()
        self.event_handler.emit_signal("open_image(QString)", image.path())

    def create_frame(self, image):
        image_frame = image.frame()
        image_frame.mouseReleaseEvent = lambda event: self.open_image(image)
        QObject.connect(self.event_handler, SIGNAL('do_search()'),
                        lambda: image.filter(self.event_handler.search_texts))
        return image_frame

    def view_person(self, person_id, person_name):
        self.open_tab(f'person_{person_id}', person_name, people_service.get_occurrences(person_id))

    def open_folder(self, path):
        self.open_tab(normalize_path(path), path.split('/').pop().split('\\').pop(), glob.glob(path + '/*'))

    def view_object(self, object_name):
        self.open_tab(f'object_{object_name}', object_name, objects_service.usage(object_name))

    def view_class(self, class_name):
        self.open_tab(f'class_{class_name}', class_name, classification_service.usage(class_name))

    def open_tab(self, path, tab_name, image_paths):
        if path in self.folders:
            idx = self.folders.index(path)
            self.setCurrentIndex(idx)
        else:
            if len(self.folders) == 0:
                self.removeTab(0)
            self.folders.append(path)
            scroll_area = QScrollArea()
            scroll_area.setWidgetResizable(False)
            layout_widget = QWidget(scroll_area)
            vbox = FlowLayout()
            images = []
            is_empty = True
            for image_path in image_paths:
                if is_image(image_path):
                    is_empty = False
                    self.images.append(image_path)
                    image = GImage(image_path, self.event_handler)
                    images.append(image)
                    image_frame = self.create_frame(image)
                    QObject.connect(self.event_handler, SIGNAL('do_search()'),
                                    lambda: image.filter(self.event_handler.search_texts))
                    image_frame.path = image_path
                    vbox.addWidget(image_frame)
                elif os.path.isdir(image_path):
                    is_empty = False
                    past = image_path.split('/').pop().split('\\').pop()
                    print(f'mostrando pasta {past}')
                    pixmap = QPixmap('resources/icons/closed_folder.png').scaledToHeight(150, Qt.SmoothTransformation)
                    label = QLabel()
                    self.link_folder(label, image_path)
                    label.setCursor(QCursor(Qt.PointingHandCursor))
                    label.setAlignment(Qt.AlignCenter)
                    label.setPixmap(pixmap)
                    folder_name = QLabel()
                    folder_name.setAlignment(Qt.AlignCenter)
                    folder_name.setText(past)
                    vlay = QVBoxLayout()
                    vlay.addWidget(label)
                    vlay.addWidget(folder_name)
                    widd = QWidget()
                    widd.setLayout(vlay)
                    vbox.addWidget(widd)
            layout_widget.setLayout(vbox)
            container = QWidget()
            container_layout = QVBoxLayout()
            actions_bar = QWidget()
            actions_bar_layout = QHBoxLayout()
            actions_bar_layout.setContentsMargins(0, 0, 10, 0)
            actions_bar.setLayout(actions_bar_layout)
            is_person = path.find('person_') == 0
            is_object = path.find('object_') == 0
            is_class = path.find('class_') == 0
            if is_person or is_object or is_class:
                if is_person:
                    label = QLabel('Visualizando Pessoa')
                elif is_class:
                    label = QLabel('Visualizando Classe')
                else:
                    label = QLabel('Visualizando Objeto')
                label.setStyleSheet('margin-left: 5px')
                actions_bar_layout.addWidget(label)
            else:
                close_button = QPushButton('Encontrar Pessoas')
                actions_bar_layout.addWidget(close_button)
                link_button(close_button, find_people, images)
                close_button = QPushButton('Encontrar Objetos')
                actions_bar_layout.addWidget(close_button)
                link_button(close_button, self.find_objects, images)
                close_button = QPushButton('Classificar Imagens')
                link_button(close_button, classify_images, images)
                actions_bar_layout.addWidget(close_button)
            close_button = QPushButton('Fechar')
            actions_bar_layout.addWidget(close_button)
            actions_bar_layout.addStretch()
            container_layout.addWidget(actions_bar)
            if is_empty:
                if is_person:
                    container_layout.addWidget(empty_folder('Não há fotos com essa pessoa.'))
                elif is_object:
                    container_layout.addWidget(empty_folder('Não há fotos com esse objeto.'))
                else:
                    container_layout.addWidget(empty_folder('Esta pasta está vazia.'))
            else:
                container_layout.addWidget(layout_widget)
                container_layout.addStretch()
            container.setLayout(container_layout)
            w = QScrollArea()
            w.setWidgetResizable(True)
            w.setWidget(container)
            self.addTab(w, tab_name)
            close_button.clicked.connect(lambda e: self.close_tab(path))
            self.setCurrentIndex(len(self.folders) - 1)

    def close_tab(self, tab_to_close):
        idx = self.folders.index(tab_to_close)
        self.folders.pop(idx)
        self.removeTab(idx)
        if len(self.folders) == 0:
            tab1 = no_folder()
            self.addTab(tab1, "Nenhuma pasta")

    def find_objects(self, images):
        news = -1
        for image in images:
            detected_objects, new_detection_object = objects_service.find(image.path())
            print('new detection', new_detection_object)
            selected_detected_objects = []
            for detected_obj in detected_objects:
                if detected_obj in self.selectable_objects:
                    selected_detected_objects.append(detected_obj)
            if new_detection_object:
                if news == -1:
                    news = 0
                news = news + len(detected_objects)
                image.add_keywords(detected_objects)
            if len(detected_objects):
                image.highlight(new_detection_object)
        msg = QMessageBox()
        msg.setWindowIcon(QIcon('resources/icons/logo_unesp.png'))
        msg.setIcon(QMessageBox.Information)
        if news > -1:
            msg.setText("Imagens processadas")
            if news == 0:
                msg.setInformativeText(f"Nenhum objeto foi encontrado")
            elif news == 1:
                msg.setInformativeText(f"1 objeto foi encontrado")
            else:
                msg.setInformativeText(f"{news} objetos foram encontrados")
        else:
            msg.setText("Todas as imagens já foram processadas")
        msg.setWindowTitle("Detecção de Objetos")
        msg.setStandardButtons(QMessageBox.Ok)
        retval = msg.exec_()


def link_button(button, function, parameter):
    button.clicked.connect(lambda e: function(parameter))


def find_people(images):
    news = -1
    for image in images:
        detected_objects, new_detection_object = people_service.find(image.path())
        print('new detection', new_detection_object)
        if new_detection_object:
            if news == -1:
                news = 0
            news = news + len(detected_objects)
            image.delete_keyword('Desconhecido')
            image.add_keywords(detected_objects)
        if len(detected_objects):
            image.highlight(new_detection_object)
    msg = QMessageBox()
    msg.setWindowIcon(QIcon('resources/icons/logo_unesp.png'))
    msg.setIcon(QMessageBox.Information)
    if news > -1:
        msg.setText("Imagens processadas")
        if news == 0:
            msg.setInformativeText('Nenhuma pessoa foi encontrada')
        elif news == 1:
            msg.setInformativeText('Uma pessoa encontrada')
        else:
            msg.setInformativeText(f"{news} pessoas foram encontradas")
    else:
        msg.setText("Todas as imagens já foram processadas")
    msg.setWindowTitle("Reconhecimento Facial")
    msg.setStandardButtons(QMessageBox.Ok)
    retval = msg.exec_()


def classify_images(images):
    news = -1
    for image in images:
        class_name, new_detection_object = classification_service.classify(image.path())
        if new_detection_object:
            if news == -1:
                news = 0
            news = news + 1
            image.add_keywords([class_name])
        image.highlight(new_detection_object)
    msg = QMessageBox()
    msg.setWindowIcon(QIcon('resources/icons/logo_unesp.png'))
    msg.setIcon(QMessageBox.Information)
    if news > -1:
        msg.setText("Imagens processadas")
        if news == 0:
            msg.setInformativeText('Não há nada de novo')
        elif news == 1:
            msg.setInformativeText('Uma imagem nova')
        else:
            msg.setInformativeText(f"{news} imagens classificadas")
    else:
        msg.setText("Todas as imagens já foram processadas")
    msg.setWindowTitle("Classificação de Imagens")
    msg.setStandardButtons(QMessageBox.Ok)
    retval = msg.exec_()


def empty_folder(text):
    tab1 = QWidget()
    tab1_layout = QVBoxLayout()
    tab1_layout.addStretch()
    pixmap = QPixmap('resources/icons/empty.png').scaledToHeight(100, Qt.SmoothTransformation)
    lbl = QLabel()
    lbl.setPixmap(pixmap)
    tab1_layout.addWidget(lbl, alignment=Qt.AlignCenter)
    lbl = QLabel(text)
    tab1_layout.addWidget(lbl, alignment=Qt.AlignCenter)
    tab1_layout.addStretch()
    tab1.setLayout(tab1_layout)
    return tab1


def no_folder():
    tab1 = QWidget()
    tab1_layout = QVBoxLayout()
    tab1_layout.addStretch()
    pixmap = QPixmap('resources/icons/folder.png').scaledToHeight(100, Qt.SmoothTransformation)
    lbl = QLabel()
    lbl.setPixmap(pixmap)
    tab1_layout.addWidget(lbl, alignment=Qt.AlignCenter)
    lbl = QLabel('Nenhuma pasta aberta.')
    tab1_layout.addWidget(lbl, alignment=Qt.AlignCenter)
    tab1_layout.addStretch()
    tab1.setLayout(tab1_layout)
    return tab1