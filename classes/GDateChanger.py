from PyQt4.QtCore import QSize, Qt, SIGNAL, QDate, QDateTime
from PyQt4.QtGui import QWidget, QLineEdit, QHBoxLayout, QIcon, QPushButton, QCursor, QLabel, QMainWindow, \
    QCalendarWidget, QVBoxLayout


class GDateChanger(QWidget):
    def __init__(self):
        super(GDateChanger, self).__init__()


        # Layout
        self.v_layout = QVBoxLayout()
        self.setLayout(self.v_layout)

        self.main_widget = QWidget()
        self.v_layout.addWidget(self.main_widget)
        self.v_layout.setMargin(0)

        # Layout Text Buttons
        self.layout = QHBoxLayout()
        self.layout.setMargin(0)
        self.main_widget.setLayout(self.layout)

        self.label = QLabel(self)
        self.label.setFixedWidth(335)
        self.layout.addWidget(self.label)

        icon = QIcon()
        icon.addFile('resources/icons/edit.png', QSize(20, 20))
        self.button = QPushButton()
        self.button.setIcon(icon)
        self.button.setContentsMargins(0, 0, 0, 0)
        self.layout.addWidget(self.button)

        icon = QIcon()
        icon.addFile('resources/icons/garbage.png', QSize(20, 20))
        self.cancel_button = QPushButton()
        self.cancel_button.setIcon(icon)
        self.cancel_button.setContentsMargins(0, 0, 0, 0)
        self.layout.addWidget(self.cancel_button)

        self.layout.addStretch()

        self.calendar = QCalendarWidget()
        self.calendar.setFixedWidth(363)
        self.calendar.setStyleSheet("""QWidget#qt_calendar_navigationbar{ background-color: #DDD}
                                                QWidget{color: black}
                                                QToolButton{ background-color: #DDD}""")
        self.calendar.hide()
        self.v_layout.insertWidget(2, self.calendar)

        # Functionality
        self.is_editing = False
        self.saved_text = ''
        self.saved_date = None
        self.button.clicked.connect(lambda event: self.toggle_editing())
        self.cancel_button.clicked.connect(lambda event: self.cancel_editing())

    def setText(self, text):
        self.label.setText(text)
        self.saved_text = text
        if self.saved_text == 'Não Definida':
            self.cancel_button.hide()
            self.label.setFixedWidth(335)
        else:
            self.cancel_button.show()
            self.label.setFixedWidth(302)

    def setDate(self, date):
        self.saved_date = date
        self.calendar.setSelectedDate(date)
        self.saved_text = f'{date.day()}/{date.month()}/{date.year()}'
        self.label.setText(self.saved_text)
        if self.saved_text == 'Não Definida':
            self.cancel_button.hide()
            self.label.setFixedWidth(335)
        else:
            self.cancel_button.show()
            self.label.setFixedWidth(302)

    def toggle_editing(self):
        if self.is_editing:
            self.stop_editing()
        else:
            self.calendar.show()
            if self.saved_date:
                self.calendar.setSelectedDate(self.saved_date)
            else:
                self.calendar.setSelectedDate(QDate.currentDate())
            self.is_editing = True
            icon = QIcon()
            icon.addFile('resources/icons/tick.png', QSize(20, 20))
            self.button.setIcon(icon)
            icon = QIcon()
            icon.addFile('resources/icons/cancel.png', QSize(20, 20))
            self.cancel_button.setIcon(icon)
            self.label.setFixedWidth(302)
            self.cancel_button.show()

    def cancel_editing(self):
        if self.is_editing:
            self.calendar.hide()
            icon = QIcon()
            icon.addFile('resources/icons/edit.png', QSize(20, 20))
            self.button.setIcon(icon)
            if self.saved_text == 'Não Definida':
                self.cancel_button.hide()
                self.label.setFixedWidth(335)
            else:
                icon = QIcon()
                icon.addFile('resources/icons/garbage.png', QSize(20, 20))
                self.cancel_button.setIcon(icon)
                self.label.setFixedWidth(302)
            self.is_editing = False
        else:
            self.saved_date = None
            self.saved_text = 'Não Definida'
            self.label.setFixedWidth(302)
            self.setText(self.saved_text)
            self.emit(SIGNAL('date_changed(QDate)'), QDate())

    def stop_editing(self):
        self.calendar.hide()

        date = self.calendar.selectedDate()
        self.saved_date = date
        self.saved_text = f'{date.day()}/{date.month()}/{date.year()}'

        self.emit(SIGNAL('date_changed(QDate)'), date)
        self.setText(self.saved_text)

        icon = QIcon()
        icon.addFile('resources/icons/edit.png', QSize(20, 20))
        self.button.setIcon(icon)
        if self.saved_text == 'Não Definida':
            self.cancel_button.hide()
            self.label.setFixedWidth(335)
        else:
            icon = QIcon()
            icon.addFile('resources/icons/garbage.png', QSize(20, 20))
            self.cancel_button.setIcon(icon)
            self.label.setFixedWidth(302)
        self.is_editing = False

    def reset(self):
        self.is_editing = False
        self.calendar.hide()
        icon = QIcon()
        icon.addFile('resources/icons/edit.png', QSize(20, 20))
        self.button.setIcon(icon)
        icon = QIcon()
        icon.addFile('resources/icons/garbage.png', QSize(20, 20))
        self.cancel_button.setIcon(icon)
        self.label.setFixedWidth(302)
        self.saved_date = None