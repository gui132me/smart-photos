import json
import pickle
from PyQt4.QtGui import QVBoxLayout, QCheckBox, QWidget, QScrollArea, QTreeWidgetItem, QTreeWidget
from PyQt4.QtCore import Qt, SIGNAL, QObject
import objects_service

event_handler = None
object_names = []
object_enabled = []
objects_total = 0
check_boxes = []
selected_objects = []
select_all_check = None
object_max = 500


def read_file():
    f = open('resources/object_detection/yolov3_open_images_ptBR.txt', 'r', encoding='utf-8')
    global object_enabled
    global object_names
    object_names = f.read().splitlines()
    try:
        file_selected_obj = open('resources/object_detection/selected_objects.pckl', 'rb')
        object_enabled = pickle.load(file_selected_obj)
        file_selected_obj.close()
    except FileNotFoundError:
        object_enabled = [Qt.Unchecked] * object_max
        new_selected_objects_file = open('resources/object_detection/selected_objects.pckl', 'wb')
        pickle.dump(object_enabled, new_selected_objects_file)
        new_selected_objects_file.close()
    for i, detection in enumerate(object_enabled):
        if detection == Qt.Checked:
            selected_objects.append(object_names[i])
    global objects_total
    objects_total = len(selected_objects)
    event_handler.emit_signal('objects_changed(QStringList)', selected_objects)


def set_checkbox_event(cb: QCheckBox, idx):
    global objects_total
    cb.stateChanged.connect(lambda event: change_selected_objects(idx, cb.checkState()))


def change_selected_objects(idx, state):
    global object_enabled
    global objects_total
    global select_all_check
    object_enabled[idx] = state
    selected_objects_file = open('resources/object_detection/selected_objects.pckl', 'wb')
    pickle.dump(object_enabled, selected_objects_file)
    selected_objects_file.close()
    if state == Qt.Checked:
        objects_total = objects_total + 1
        if objects_total == object_max:
            select_all_check.setCheckState(Qt.Checked)
        selected_objects.append(object_names[idx])
    else:
        objects_total = objects_total - 1
        select_all_check.disableChange = True
        select_all_check.setCheckState(Qt.Unchecked)
        remove_object(idx)
    event_handler.emit_signal('objects_changed(QStringList)', selected_objects)


def remove_object(idx):
    obj_to_remove = selected_objects.index(object_names[idx])
    del selected_objects[obj_to_remove]


def set_sel_all_checkbox_event():
    global select_all_check
    select_all_check.stateChanged.connect(lambda event: select_all_checkbox_event())


def select_all_checkbox_event():
    global select_all_check
    global objects_total
    if select_all_check.checkState() == Qt.Checked:
        for cb in check_boxes:
            cb.setCheckState(Qt.Checked)
    else:
        if objects_total == object_max:
            for cb in check_boxes:
                cb.setCheckState(Qt.Unchecked)


def create_objects_list(main_window_event_handler):
    global event_handler
    event_handler = main_window_event_handler
    read_file()
    objects_layout = QVBoxLayout()
    global check_boxes
    check_boxes = []
    global select_all_check
    select_all_check = QCheckBox()
    select_all_check.setText('Selecionar Tudo')
    if objects_total == object_max:
        select_all_check.setCheckState(Qt.Checked)
    else:
        select_all_check.setCheckState(Qt.Unchecked)
    set_sel_all_checkbox_event()
    objects_layout.addWidget(select_all_check)
    idx = 0
    for obj in object_names:
        obj_check = QCheckBox(obj)
        obj_check.setText(obj)
        check_boxes.append(obj_check)
        obj_check.setCheckState(object_enabled[idx])
        if object_enabled[idx]:
            selected_objects.append(obj)
        obj_check.setStyleSheet('margin: 0 0 0 10px')
        set_checkbox_event(obj_check, idx)
        objects_layout.addWidget(obj_check)
        idx = idx + 1
    event_handler.emit_signal('objects_changed(QStringList)', selected_objects)
    global tree
    tree = QTreeWidget()
    headerItem = QTreeWidgetItem()
    headerItem.setText(0, "Objetos: Dois cliques para visualizar")
    tree.setHeaderItem(headerItem)

    # with open('image-labels-transformed.json', 'r') as f:
    with open('image-labels-structure-pt.json', 'r') as f:
        itens_dict = json.load(f)
    global all_itens
    all_itens = {}
    for item_object in itens_dict:
        if item_object['name'] == 'Todos' or item_object['name'] == 'Object':
            item = QTreeWidgetItem(tree)
            item.setExpanded(True)
            item.setFlags(item.flags() | Qt.ItemIsTristate | Qt.ItemIsUserCheckable)
            item.setText(0, 'Todos')
            item.selection_id = -1
        else:
            item = QTreeWidgetItem(all_itens[item_object['parent']])
            item.setText(0, item_object['name'])
            item.setFlags(item.flags() | Qt.ItemIsTristate | Qt.ItemIsUserCheckable)
            if item_object['class']:
                item.setText(0, f'{item_object["name"]} ({len(objects_service.usage(item_object["name"]))})')
                item.selection_id = item_object['selection_id']
                item.setCheckState(0, object_enabled[item.selection_id])
                item.emitDataChanged()
                all_itens[item_object['name']] = item
            else:
                item.selection_id = -1
                item.setCheckState(0, Qt.Unchecked)


            # item.setCheckState(0, Qt.Unchecked)
        all_itens[item_object['id']] = item

    tree.itemChanged.connect(connect_data_changed)
    tree.doubleClicked.connect(item_clicked)
    tree.connect(event_handler, SIGNAL('new_objects(QStringList)'), objects_found)

    tree.sortByColumn(0, Qt.AscendingOrder)
    return tree


def objects_found(object_list):
    global all_itens
    for obj in object_list:
        all_itens[obj].setText(0, f'{obj} ({len(objects_service.usage(obj))})')


def item_clicked(tree_item_index):
    global tree
    tree_item = tree.itemFromIndex(tree_item_index)
    if tree_item.selection_id is not -1:
        print(f'{object_names[tree_item.selection_id]} was clicked')
        # change_selected_objects(tree_item.selection_id, tree_item.checkState(0))
        event_handler.emit_signal('view_object(QString)', object_names[tree_item.selection_id])


def connect_data_changed(tree_item, object):
    if tree_item.selection_id is not -1:
        print(f'{object_names[tree_item.selection_id]} changed to {tree_item.checkState(0)}')
        change_selected_objects(tree_item.selection_id, tree_item.checkState(0))