import os
from PyQt4.QtGui import QFileSystemModel, QTreeView
from PyQt4.QtCore import Qt, QDir

event_handler = None


class GFileSystemModel(QFileSystemModel):
    def __init__(self):
        super(GFileSystemModel, self).__init__()

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if section == 0 and role == Qt.DisplayRole:
            return 'Pastas do Sistema'
        return super(QFileSystemModel, self).headerData(section, orientation, role)


model = GFileSystemModel()


def is_image(path):
    path = path.lower()
    extensions = [".jpg", ".jpeg", ".png", ".gif"]
    for ext in extensions:
        if path.endswith(ext):
            return True
    return False


def select_file_item(index):
    path = model.filePath(index)
    if os.path.isfile(path):
        if is_image(path):
            open_image(path)
        else:
            pass
    elif os.path.isdir(path):
        open_folder(path)


def open_image(image_path):
    event_handler.emit_signal("open_image(QString)", image_path)


def open_folder(folder_path):
    event_handler.emit_signal("open_folder(QString)", folder_path)


def create_system_explorer(main_window_event_handler):
    global event_handler
    event_handler = main_window_event_handler
    model.setRootPath(QDir.homePath())
    model.headerData(0, Qt.Horizontal)
    tree = QTreeView()
    tree.setModel(model)
    tree.setRootIndex(model.index(QDir.homePath()))
    tree.hideColumn(4)
    tree.hideColumn(3)
    tree.hideColumn(2)
    tree.hideColumn(1)
    tree.clicked.connect(select_file_item)
    return tree
