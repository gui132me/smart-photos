import sqlite3
import os.path
from PyQt4.QtCore import QObject, SIGNAL

import object_detection as od


selected_objects = []


def create_database():
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS OBJECT
                     ( ID INTEGER PRIMARY KEY NOT NULL,
                     IMAGE TEXT NOT NULL,
                     OBJ TEXT
                     );''')
    conn.commit()
    cursor.close()


def check_system():
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    query = 'SELECT IMAGE FROM OBJECT'
    result = cursor.execute(query)
    print(result)
    for row in result:
        image = row[0]
        print('checking', image)
        if not os.path.exists(image):
            delete_cursor = conn.cursor()
            query = 'DELETE FROM OBJECT WHERE IMAGE=?'
            delete_cursor.execute(query, [image])
            print(image, 'deleted')
    conn.commit()
    cursor.close()


def start(main_event_handler):
    global listener
    global event_handler
    event_handler = main_event_handler
    listener = QObject()
    listener.connect(main_event_handler, SIGNAL('image_renamed(QStringList)'), rename)
    create_database()
    check_system()
    od.start()


def rename(paths):
    old_file_path = paths[0]
    new_file_path = paths[1]
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    query = 'UPDATE OBJECT SET IMAGE = ? WHERE IMAGE = ?'
    cursor.execute(query, [new_file_path, old_file_path])
    conn.commit()
    cursor.close()


def find(image_path):
    query = 'SELECT OBJ FROM OBJECT WHERE IMAGE = ?'
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    objects = []
    results = cursor.execute(query, [image_path])
    for row in results:
        if row[0] is None:
            return objects, False
        objects.append(row[0])
    conn.commit()
    cursor.close()

    if len(objects):
        return objects, False

    objects = od.detect(image_path)
    insert_objects(image_path, objects)
    global event_handler
    event_handler.emit_signal('new_objects(QStringList)', objects)
    return objects, True


def insert_objects(image_path, objects):
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    if len(objects):
        query = 'INSERT INTO OBJECT (IMAGE, OBJ) VALUES '
        first_tuple = True
        tuples_args = []
        for obj in objects:
            if first_tuple:
                first_tuple = False
                query = query + '(?, ?)'
            else:
                query = query + ', (?, ?)'
            tuples_args.append(image_path)
            tuples_args.append(obj)
        query = query + ';'
        cursor.execute(query, tuples_args)
    else:
        query = 'INSERT INTO OBJECT (IMAGE, OBJ) VALUES (?, NULL)'
        cursor.execute(query, [image_path])
    conn.commit()
    cursor.close()


def usage(object_name):
    conn = sqlite3.connect('database.sqlite')
    cursor = conn.cursor()
    query = 'SELECT IMAGE FROM OBJECT WHERE OBJ = ?'
    result = cursor.execute(query, [object_name])
    images = []
    for row in result:
        images.append(row[0])
    conn.commit()
    cursor.close()
    return images
