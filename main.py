import sys
from PyQt4.QtGui import QApplication

import classification_service
import location_service
import objects_service
import people_service
from classes.GEvent import GEvent
from classes.GMainWindow import GMainWindow

if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setCursorFlashTime(0)
    event_handler = GEvent()
    classification_service.start()
    location_service.start()
    people_service.start(event_handler)
    objects_service.start(event_handler)
    main_window = GMainWindow(event_handler)
    # event_handler.emit_signal('open_folder(QString)', 'C:/Users/gui13/Pictures/Open Images Dataset')
    sys.exit(app.exec_())
