import face_recognition as fr
import os
import cv2
import face_recognition
import numpy as np
from time import sleep
import pickle


faces = None


def get_encoded_faces():
    """
    looks through the faces folder and encodes all
    the faces

    :return: dict of (name, image encoded)
    """

    f = open('./faces/encoded.pckl', 'rb')
    encoded = pickle.load(f)
    f.close()
    return encoded

    encoded = {}

    for dirpath, dnames, fnames in os.walk("./faces"):
        for f in fnames:
            if f.endswith(".jpg") or f.endswith(".png"):
                face = fr.load_image_file("faces/" + f)
                encodings = fr.face_encodings(face, num_jitters=10)
                if len(encodings):
                    encoding = encodings[0]
                    encoded[f.split(".")[0]] = encoding
                else:
                    print(f'nao encontrei o rosto de {f}')

    f = open('./faces/encoded.pckl', 'wb')
    pickle.dump(encoded, f)
    f.close()

    return encoded


def get_first_encoding(img_path):
    face = fr.load_image_file(img_path)
    encoding = fr.face_encodings(face)[0]
    return encoding


def add_encoding(img):
    """
    encode a face given the file name
    """
    # face = fr.load_image_file("faces/" + img)
    # encoding = fr.face_encodings(face)[0]

    face = fr.load_image_file("faces/" + img)
    encodings = fr.face_encodings(face, num_jitters=10)

    global faces
    faces[img.split(".")[0]] = encodings

    f = open('./faces/encoded.pckl', 'wb')
    pickle.dump(faces, f)
    f.close()


def restart():
    print('face 1')
    global faces_encoded
    global known_face_names
    global faces
    faces_encoded = list(faces.values())
    known_face_names = list(faces.keys())
    print('face 2')


def start(event_handler):
    print('face 1')
    global faces_encoded
    global known_face_names
    global faces
    faces = get_encoded_faces()
    faces_encoded = list(faces.values())
    known_face_names = list(faces.keys())
    event_handler.emit_signal('people_changed(QStringList)', known_face_names)
    print('face 2')


def detect(im, known_faces):
    print('face 3', im)

    faces_encoded = list(known_faces.values())
    known_face_names = list(known_faces.keys())

    # img = cv2.imread(im, 1)
    stream = open(im, "rb")
    img_bytes = bytearray(stream.read())
    numpyarray = np.asarray(img_bytes, dtype=np.uint8)
    img = cv2.imdecode(numpyarray, cv2.IMREAD_UNCHANGED)

    try:
        width = img.shape[1]
        height = img.shape[0]
    except:
        return []

    size = 512
    if width >= height:
        ratio = height / width
        img = cv2.resize(img, (size, int(size * ratio)),interpolation = cv2.INTER_AREA)
    else:
        ratio = width / height
        img = cv2.resize(img, (int(size * ratio), size),interpolation = cv2.INTER_AREA)

    # cv2.imshow(' ', img)
    # cv2.waitKey()

    face_locations = face_recognition.face_locations(img)
    unknown_face_encodings = face_recognition.face_encodings(img, face_locations)

    face_names = []
    for face_encoding in unknown_face_encodings:
        # See if the face is a match for the known face(s)
        matches = face_recognition.compare_faces(faces_encoded, face_encoding)
        name = 0

        # use the known face with the smallest distance to the new face
        face_distances = face_recognition.face_distance(faces_encoded, face_encoding)
        best_match_index = np.argmin(face_distances)
        if matches[best_match_index]:
            name = known_face_names[best_match_index]

        face_names.append(name)

        # for (top, right, bottom, left), name in zip(face_locations, face_names):
        #     # Draw a box around the face
        #     cv2.rectangle(img, (left - 20, top - 20), (right + 20, bottom + 20), (255, 0, 0), 2)
        #
        #     # Draw a label with a name below the face
        #     cv2.rectangle(img, (left - 20, bottom - 15), (right + 20, bottom + 20), (255, 0, 0), cv2.FILLED)
        #     font = cv2.FONT_HERSHEY_DUPLEX
        #     cv2.putText(img, name, (left - 20, bottom + 15), font, 1.0, (255, 255, 255), 2)

    print('face 4')
    return face_names


def get_names():
    global known_face_names
    return known_face_names


def delete(name):
    global faces
    global faces_encoded
    global known_face_names
    if name in faces:
        del faces[name]
        faces_encoded = list(faces.values())
        known_face_names = list(faces.keys())
        save()


def save():
    global faces
    f = open('./faces/encoded.pckl', 'wb')
    pickle.dump(faces, f)
    f.close()
